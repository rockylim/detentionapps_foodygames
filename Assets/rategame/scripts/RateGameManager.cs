﻿using System;
using Caviezel.Audio;
using Caviezel.Core;
using Caviezel.Pretend;
using Caviezel.Serialization;
using Caviezel.Tween;
using UnityEngine;


namespace RateGameSystem {
  public class RateGameManager : MonoBehaviour, ISystem {
    public GameObject m_viewObj;
    public GameObject m_buttonClose;
    public GameObject m_buttonYes;
    public GameObject m_buttonNo;
    public int m_showCounter;
    public int[] m_skipInterstitialIndexScene;

    [Header("Rate Game URL")]
    public string m_rateGameUrlIOS;
    public string m_rateGameUrlAndroid;
    public string m_rateGameUrlAmazon;

    CTweenChainer m_buttonPressTweener;
    CTweenChainer m_visibilityTweener;
    SfxManager m_sfxManager;
    GamePersisters m_gamePersisters;
    RatePersister m_persister;



    #region ISystem implementation
    public void InitSystem() {
    }

    public void StartSystem(CavEngine engine) {
      m_sfxManager = engine.GetSystem<AudioSystem>().GetManager<SfxManager>();
      Debug.Assert(null != m_sfxManager);

      m_gamePersisters = engine.GetSystem<GamePersisters>();
      Debug.Assert(null != m_gamePersisters);

      m_persister = m_gamePersisters.GetPersister<RatePersister>();
      Debug.Assert(null != m_persister);

      m_buttonPressTweener = new CTweenChainer();
      m_buttonPressTweener.Add(m_buttonClose, true);
      m_buttonPressTweener.Add(m_buttonYes, true);
      m_buttonPressTweener.Add(m_buttonNo, true);

      m_visibilityTweener = new CTweenChainer();
      m_visibilityTweener.Add(m_viewObj, true);
    }

    public void UpdateSystem(float dt) {
      m_buttonPressTweener.Update(dt);
      m_visibilityTweener.Update(dt);
    }

    public void OnChangeScene(int index) {
      AddCount();
      if (!Array.Exists(m_skipInterstitialIndexScene, x => x == index)) {
        Show(true);
      }
    }
    #endregion

    #region Public Methods
    public bool IsExceedCounter() {
      return (m_persister.RateGameCount >= m_showCounter);
    }

    public void Show(bool checkCounter) {
      if (m_persister.StopRateGame) {
        return;
      }

      if (checkCounter) {
        if (m_persister.RateGameCount < m_showCounter) {
          return;
        }
      }
      m_persister.RateGameCount = 0;
      m_viewObj.SetActive(true);
      m_visibilityTweener.PlayExistingTween(new string[] { "tw_show" }, () => { });
    }

    public void Hide() {
      m_viewObj.SetActive(false);
      m_visibilityTweener.PlayExistingTween(new string[] { "tw_hide" }, () => { });
    }

    public void OnButtonPress(string buttonName) {
      m_sfxManager.PlaySound("pop");
      m_buttonPressTweener.PlayExistingTween(new string[] { "onpress" + buttonName }, () => {
        Hide();
        if (buttonName == "ok" || buttonName == "no") {
          m_persister.StopRateGame = true;
          m_gamePersisters.SaveAll();
          if (buttonName == "ok") {
#if UNITY_ANDROID
            if (Constants.IsAmazon) {
              Application.OpenURL(m_rateGameUrlAmazon);
            } else {
              Application.OpenURL(m_rateGameUrlAndroid);
            }

#elif UNITY_IPHONE
                        Application.OpenURL(m_rateGameUrlIOS);
#endif
          }
        }
      });
    }


    public void AddCount() {
      m_persister.RateGameCount++;
    }
    #endregion
  }
}

