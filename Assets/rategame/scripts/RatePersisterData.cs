﻿using System;

namespace RateGameSystem {
    [Serializable]
    public class RatePersisterData {
        public bool m_stopRateGame;
        public int m_counter;

        public RatePersisterData() {
            ResetData();
        }

        public void ResetData() {
            m_stopRateGame = false;
            m_counter = 0;
        }
    }
}