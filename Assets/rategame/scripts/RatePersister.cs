﻿using System.IO;
using UnityEngine;
using Caviezel.Serialization;

namespace RateGameSystem {
    public class RatePersister : MonoBehaviour, IPersister<RatePersisterData> {
        RatePersisterData m_data;

        public bool StopRateGame {
            get { return m_data.m_stopRateGame; }
            set { m_data.m_stopRateGame = value; }
        }

        public int RateGameCount {
            get { return m_data.m_counter; }
            set { m_data.m_counter = value; }
        }

        public void FlushSaveObjects() {
            m_data.ResetData();
        }

        public static string GetPath() {
            return Path.Combine(Application.persistentDataPath, "ratedata.txt");
        }

        #region IPersister
        public void Init(IDataSerializer serializer) {
            m_data = serializer.Load<RatePersisterData>(GetPath());
            if (null == m_data) {
                Debug.Log("ptd data doesnt exist, create new");
                m_data = new RatePersisterData();
                serializer.Save(m_data, GetPath());
            }
        }

        public void Reset(IDataSerializer serializer) { }

        public void Save(IDataSerializer serializer) {
            serializer.Save(m_data, GetPath());
        }

        public void StartPersister(GamePersisters persisters) {
        }
        #endregion
    }
}