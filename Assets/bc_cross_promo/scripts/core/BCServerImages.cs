﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Caviezel.Pretend;
using UnityEngine;

namespace Brainfull.CrossPromo {
  public class BCServerImages : MonoBehaviour {
    public delegate void EvDownloaded(List<Item> downloaded);

    [Serializable]
    public class Item {
      public string name;
      public string icon;
      public string thumbnail;
      // ios url
      public string ios;
      // google play url
      public string google;
      // amazon url
      public string amazon;

      public bool Valid {
        get {
          return !name.IsEmpty() &&
          !ios.IsEmpty() &&
          !google.IsEmpty() &&
          !amazon.IsEmpty();
        }
      }

      public Texture2D GetIconTexture() {
        return GetTexture(icon.LastSplit());
      }

      public Texture2D GetThumbTexture() {
        return GetTexture(thumbnail.LastSplit());
      }

      public bool IconExists() {
        string filePath = $"{Application.persistentDataPath}/{icon.LastSplit()}";
        return File.Exists(filePath);
      }

      public bool ThumbExists() {
        string filePath = $"{Application.persistentDataPath}/{thumbnail.LastSplit()}";
        return File.Exists(filePath);
      }

      Texture2D GetTexture(string path) {
        string filePath = $"{Application.persistentDataPath}/{path}";
        if (File.Exists(filePath)) {
          return filePath.ToTexture2D();
        }
        return null;
      }
    }

    [Serializable]
    public class Showing {
      public List<Item> items = new List<Item>();
    }

    [Tooltip("the folder where the server images are located at.")]
    public string Prefix;
    public string SpecialPrefixForGooglePlay;
    public string SpecialPrefixForAmazon;
    public string SpecialPrefixForIOS;

    JsonDownloader<Showing> _showingDownloader = new JsonDownloader<Showing>();
    ImageDownloader _imgDownloader = new ImageDownloader();
    Showing _showing = null;
    string _url = "https://cf-simple-s3-origin-hog-284761918830.s3-us-west-1.amazonaws.com/";

    public EvDownloaded OnDownloaded { get; set; }
    public string BaseUrl {
      get {
        return $"{_url}{Prefix}";
      }
    }

    public IEnumerator InitServerImages() {

#if UNITY_IOS
      if (!SpecialPrefixForIOS.IsEmpty()) {
        Prefix = SpecialPrefixForIOS;
      }
#else
      if (!Constants.IsAmazon) {
        if (!SpecialPrefixForGooglePlay.IsEmpty()) {
          Prefix = SpecialPrefixForGooglePlay;
        }
      } else {
        if (!SpecialPrefixForAmazon.IsEmpty()) {
          Prefix = SpecialPrefixForAmazon;
        }
      }
#endif

      string jsonUrl = $"{BaseUrl}/showing.json";
      yield return _showingDownloader.Download(jsonUrl, (mdl, err) => {
        if (!err) {
          _showing = mdl;
        } else {
          Print.Log($"error when downloading {jsonUrl}");
        }
      });

      foreach (Item item in _showing.items) {
        if (!item.IconExists()) {
          yield return _imgDownloader.Download(BaseUrl, item.icon, success => { });
        }

        if (!item.thumbnail.IsEmpty() && !item.ThumbExists()) {
          yield return _imgDownloader.Download(BaseUrl, item.thumbnail, success => { });
        }
      }

      OnDownloaded?.Invoke(_showing.items);
    }
  }
}

