﻿using Caviezel;
using Caviezel.Tween;
using UnityEngine;
using UnityEngine.UI;

namespace Brainfull.CrossPromo {
  using Item = BCCrossPromo.CrossPromoItem;

  public class BCIcon : MonoBehaviour {
    public RawImage Img;

    Item _item;
    ITween _tween;

    public void InitIcon(Item it) {
      _item = it;

      Img.texture = _item.Model.GetIconTexture();
      _tween = GetComponent<ITween>();
      _tween.Setup();
      _tween.Play();
    }

    public void UpdateIcon(float dt) {
      _tween.UpdateTween(dt);
    }

    public void OnPress() {
      Application.OpenURL(_item.Url);
    }
  }
}


