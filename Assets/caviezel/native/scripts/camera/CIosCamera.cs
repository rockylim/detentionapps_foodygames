﻿using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Caviezel {
    namespace Native {

        public class CIosCamera : ICamera {
#if UNITY_IOS
            [DllImport("__Internal")]
            static extern void _Cav_SaveToCameraRoll(string encodedMedia);
#endif
            public CIosCamera() { }

            #region ICamera
            public IEnumerator CaptureScreen() {
                yield return new WaitForEndOfFrame();
                // Create a texture the size of the screen, RGB24 format
                int width = Screen.width;
                int height = Screen.height;
                Texture2D texture = new Texture2D(width, height, TextureFormat.RGB24, false);
                // Read screen contents into the texture
                texture.ReadPixels(new Rect(0, 0, width, height), 0, 0);
                texture.Apply();
                //save to camera roll
                if (texture != null) {
#if UNITY_IOS
                    byte[] val = texture.EncodeToPNG();
                    string bytesString = System.Convert.ToBase64String(val);
                    _Cav_SaveToCameraRoll(bytesString);
#endif
                }
                //destroy the texture
                Texture2D.DestroyImmediate(texture, true);
            }
            #endregion
        }

    }
}