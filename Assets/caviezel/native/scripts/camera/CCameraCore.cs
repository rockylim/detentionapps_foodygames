﻿using System.Collections;

namespace Caviezel {
    namespace Native {
        public interface ICamera {
            IEnumerator CaptureScreen();
        }
    }
}