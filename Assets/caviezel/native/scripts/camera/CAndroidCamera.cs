﻿using System.Collections;
using UnityEngine;

namespace Caviezel {
    namespace Native {
        public class CAndroidCamera : ICamera {
            string m_fileName;

            public CAndroidCamera(string fileName) {
                m_fileName = fileName;
            }

            #region ICamera
            public IEnumerator CaptureScreen() {
                yield return new WaitForEndOfFrame();
                // Create a texture the size of the screen, RGB24 format
                int width = Screen.width;
                int height = Screen.height;
                Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, false);
                // Read screen contents into the texture
                tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
                tex.Apply();

                if (tex != null) {
                    byte[] val = tex.EncodeToPNG();
                    string imgData = System.Convert.ToBase64String(val);
                    string filename = m_fileName + System.Guid.NewGuid() + ".png";

                    AndroidJavaClass playerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                    AndroidJavaObject activity = playerClass.GetStatic<AndroidJavaObject>("currentActivity");
                    AndroidJavaClass pluginClass = new AndroidJavaClass("com.digitify.unity.camera.DigitCamera");
                    pluginClass.CallStatic("SaveScreenshot", activity, imgData, filename);
                }
            }
            #endregion
        }       
    }
}
