﻿#if UNITY_IOS
using UnityEditor;
using UnityEditor.Callbacks;
using System.IO;
using UnityEditor.iOS.Xcode;
#endif

namespace Caviezel {
    namespace Native {
        public class NativePostBuildProcessor {
            const string m_photoLibUsageDesc = "Save media to Photos";

            #if UNITY_IOS
            [PostProcessBuild]
            public static void OnPostprocessBuild(BuildTarget target, string buildPath) {                
                if (target == BuildTarget.iOS) {
                    string plistPath = Path.Combine(buildPath, "Info.plist");

                    PlistDocument plist = new PlistDocument();
                    plist.ReadFromString(File.ReadAllText(plistPath));

                    PlistElementDict rootDict = plist.root;
                    rootDict.SetString("NSPhotoLibraryAddUsageDescription", m_photoLibUsageDesc);

                    File.WriteAllText(plistPath, plist.WriteToString());
                }
            }
            #endif
        }       
    }
}
