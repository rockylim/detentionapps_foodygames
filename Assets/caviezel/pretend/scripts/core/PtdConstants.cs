﻿using UnityEngine;

namespace Caviezel {
  namespace Pretend {
    public class Constants {
      #region Debugging
      public static readonly bool IsDebugMode = true;
      #endregion

      public static readonly Vector2 TapAreaThreshold = new Vector2(50f, 50f);
      public static readonly bool IsAmazon = false;
    }
  }
}