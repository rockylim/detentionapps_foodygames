﻿using UnityEngine;
using Caviezel.Core;
using Caviezel.Audio;
using Caviezel.Chrono;

namespace Caviezel {
    namespace Pretend {
        [DisallowMultipleComponent]
        public class PtdTapCloneable : PtdComponentActive {
            public RectTransform m_clonedArea;

            PtdTapCloneableCore m_controller;
            RectTransform m_rectTransform;


            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);

                //handle the tap
                Owner.InitGestureHandler();
                Owner.Gestures.SetTappable();
                Owner.Gestures.OnTapListeners += OnTap;

                m_rectTransform = GetComponent<RectTransform>();

                if (m_clonedArea == null) {
                    m_clonedArea = m_rectTransform;
                }
            }

            public void InitController(PtdTapCloneableCore controller) {
                m_controller = controller;
            }

            public RectTransform GetClonedArea() {
                return m_clonedArea;
            }

            public RectTransform GetRect() {
                return m_rectTransform;
            }

            void OnTap(Vector2 pos) {
                if (IsTappable()) {
                    m_controller.ClonedOnTarget(m_clonedArea, pos);
                }
            }

            bool IsTappable() {
                if (!IsActive) {
                    return false;
                }
                return true;
            }
        }
    }
}
