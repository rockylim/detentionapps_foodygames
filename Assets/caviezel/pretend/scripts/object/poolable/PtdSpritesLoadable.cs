﻿using System;
using System.Collections.Generic;
using Caviezel.Core;
using UnityEngine;
using UnityEngine.UI;


namespace Caviezel {
  namespace Pretend {

    public class PtdSpritesLoadable : PtdComponentActive {
      [Serializable]
      public struct PositionData {
        public bool m_shouldChangePosition;
        public Vector2 m_position;
      }

      public BaseReceiverEv[] m_onShouldLoadSpriteEv;
      public List<Sprite> m_spriteList;
      public List<PositionData> m_spritePosition;
      public Image m_targetImage;
      public float m_nativeSizeRatio;

      public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
        base.InitComponent(sceneStarter, gameController);
        if (m_targetImage == null) {
          m_targetImage = GetComponent<Image>();
        }
      }
      public override void OnStartGame() {
        base.OnStartGame();
      }


      public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
        base.OnReceiveEvent(ev, scope);
        if (IsActive) {
          for (int i = 0; i < m_onShouldLoadSpriteEv.Length; ++i) {
            if (m_onShouldLoadSpriteEv[i].Matches(ev, scope)) {
              int index = -1;
              string nameIndex = ev.m_eventId.Substring(m_onShouldLoadSpriteEv[i].m_eventId.Length);
              index = GetSpriteIndex(nameIndex);
              if (index == -1) {
                return true;
              }

              m_targetImage.sprite = m_spriteList[index];
              if (m_spritePosition[index].m_shouldChangePosition) {
                m_targetImage.rectTransform.SetPos(m_spritePosition[index].m_position);
              }

              if (m_nativeSizeRatio > 0f) {
                m_targetImage.SetNativeSize();
                m_targetImage.rectTransform.SetSize(new Vector2(m_targetImage.rectTransform.Width() * m_nativeSizeRatio, m_targetImage.rectTransform.Height() * m_nativeSizeRatio));
              }
              return true;
            }
          }
        }
        return false;
      }

      int GetSpriteIndex(string nameIndex) {        
        int nameIndexLen = nameIndex.Length;
        for (int i = 0; i < m_spriteList.Count; i++) {
          if (m_spriteList[i].name.Substring(m_spriteList[i].name.Length - nameIndexLen, nameIndexLen) == nameIndex) {
            return i;
          }
        }
        return -1;
      }
    }
  }
}
