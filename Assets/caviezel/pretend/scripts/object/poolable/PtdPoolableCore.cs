﻿using System;
using UnityEngine;

namespace Caviezel {
    namespace Pretend {
        [Serializable]
        public class PoolItemData {
            public PtdObject m_item;
            public BaseSenderEv m_onReleaseEv;

            Vector2 m_originPos;
            float m_originRotation;
            PtdObject m_originParent;
            Transform m_originTrasformParent;

            public void InitPoolItem() {
                RectTransform itemRt = m_item.RootTransform;
                m_originPos = itemRt.Pos();
                m_originRotation = itemRt.Rotation();
                m_originTrasformParent = itemRt.parent;
                m_originParent = m_item.Parent;
            }

            public void Release() {
                m_item.gameObject.SetActive(true);
                IsReleased = true;
            }

            public void Retain(bool showOnStart) {                
                //set back to default
                RectTransform itemRt = m_item.RootTransform;
                itemRt.SetParent(m_originTrasformParent);
                itemRt.SetPos(m_originPos);
                itemRt.SetRotation(m_originRotation);
                itemRt.SetScale(Vector2.one);
                //iobject related
                m_item.SetParent(m_originParent);
                m_item.gameObject.SetActive(showOnStart);
                IsReleased = false;
            }

            public bool IsReleased { get; private set; }
        }
    }
}
