﻿using UnityEngine;
namespace Caviezel {
    namespace Pretend {
        public class PtdTrackString : PtdTrackerBase {
            public TrackerEv[] m_trackEvs;
            public BaseReceiverEv[] m_trackTriggerEv;

            public override void Track() {
                for (int i = 0; i < m_trackEvs.Length; ++i) {
                    SaveObjectData data = m_trackEvs[i].m_isTemporary ? m_persister.GetTemp() : m_persister.GetRecord();
                    string record;
                    //check whether the record exist in the persistent data
                    if (data.TryGetString(m_trackEvs[i].m_recordId, out record)) {
                        //broadcast
                        BroadcastEvent(new BaseSenderEv(record));
                    } else {
                        BroadcastEvent(m_trackEvs[i].m_onNotFoundEv);
                    }
                }
            }

            public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {
                    if (m_trackTriggerEv != null) {
                        for (int i = 0; i < m_trackTriggerEv.Length; ++i) {
                            if (m_trackTriggerEv[i].Matches(ev, scope)) {
                                Track();
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
        }
    }
}
