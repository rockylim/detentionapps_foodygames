﻿namespace Caviezel {
    namespace Pretend {
		public class PtdTrackNumber : PtdTrackerBase {  
			public TrackNumberEv[] m_trackEvs;

            public override void Track() {
                for (int i = 0; i < m_trackEvs.Length; ++i) {
                    SaveObjectData data = m_trackEvs[i].m_isTemporary ? m_persister.GetTemp() : m_persister.GetRecord();
                    int recCount;
                    //check whether the record exist in the persistent data
                    if (data.TryGetInt(m_trackEvs[i].m_recordId, out recCount)) {
                        //check whether the record has exceeded the threshold
                        if (recCount >= m_trackEvs[i].m_threshold) {
                            //keep broadcasting if repeatable
                            if (m_trackEvs[i].m_isRepeatable) {
                                BroadcastEvent(m_trackEvs[i].m_onDoneEv);
                            } else {
                                SaveObjectData processedRecord = m_persister.GetProcessedRecord();
                                int processedRecCount;
                                //if not repeatable, check if exist in the processed rec, if not found then broadcast
                                if (!processedRecord.TryGetInt(m_trackEvs[i].m_recordId, out processedRecCount)) {
                                    processedRecord.AddPair(new KVData(m_trackEvs[i].m_recordId, recCount));
                                    BroadcastEvent(m_trackEvs[i].m_onDoneEv);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}