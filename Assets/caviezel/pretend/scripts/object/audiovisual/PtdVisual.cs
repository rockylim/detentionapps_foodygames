﻿using System.Collections.Generic;
using UnityEngine;

namespace Caviezel {
    namespace Pretend {
        [DisallowMultipleComponent]
        public class PtdVisual : PtdComponentActive {
            public VisualEv[] m_visualEvs;

			public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {
                    for (int i = 0; i < m_visualEvs.Length; ++i) {
                        if (m_visualEvs[i].Matches(ev, scope)) {
                            PlayTween(m_visualEvs[i]);
                            return true;
                        }
                    }
                }
                return false;
			}

            void PlayTween(VisualEv ev) {
                HashSet<string> tweenIds = new HashSet<string>();
                for (int i = 0; i < ev.m_data.Length; ++i) {
                    TweenableData tweenableData = ev.m_data[i];
                    if (!string.IsNullOrEmpty(tweenableData.m_tweenId)) {
                        if (!tweenableData.m_shouldStop) {
                            if (!Owner.Tweener.IsTweenPlaying(tweenableData.m_tweenId)) {
                                tweenIds.Add(tweenableData.m_tweenId);
                            }
                        } else {
                            Owner.Tweener.StopTween(tweenableData.m_tweenId);
                            BroadcastEvent(ev.m_onDoneTweenEv);
                        }
                    }   
                }
                //tell owner to play the tweens, if any
                if (tweenIds.Count > 0) {                    
                    Owner.Tweener.PlayExistingTween(tweenIds, () => {
                        BroadcastEvent(ev.m_onDoneTweenEv);
                    });   
                }
            }
		}       
    }
}
