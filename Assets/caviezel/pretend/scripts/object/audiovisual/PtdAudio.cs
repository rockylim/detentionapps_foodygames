﻿using UnityEngine;
using Caviezel.Audio;
using Caviezel.Core;

namespace Caviezel {
    namespace Pretend {
        public class PtdAudio : PtdComponentActive {
            public AudioEv[] m_audioEvs;
 
            SfxManager m_sfxManager;

            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
                //cache the sfx manager
                m_sfxManager = sceneStarter.Engine.GetSystem<AudioSystem>().GetManager<SfxManager>();
                Debug.Assert(null != m_sfxManager);
            }

            public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {
                    for (int i = 0; i < m_audioEvs.Length; ++i) {
                        if (m_audioEvs[i].Matches(ev, scope)) {
                            m_sfxManager.PlaySound(m_audioEvs[i].m_sfxs);   
                            return true;
                        }
                    }
                }
                return false;
            }
        }       
    }
}
