﻿using Caviezel.Audio;
using Caviezel.Core;
using Detentionapps.FoodyGames;
using UnityEngine;

namespace Caviezel {
  namespace Pretend {
    public class PtdStopAllSfx : PtdComponentActive {
      public BaseReceiverEv m_onStopAllSfx;
      public bool m_isFiltered;
      [ConditionalHide("m_isFiltered", true)]
      public string[] m_filteredSounds;

      SfxManager m_sfxManager;
      FoodyGameSystem m_ptdSystem;


      #region PtdComponentActive
      public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
        base.InitComponent(sceneStarter, gameController);

        // get main system
        m_ptdSystem = sceneStarter.Engine.GetSystem<FoodyGameSystem>();
        Debug.Assert(null != m_ptdSystem);

        //cache the sfx manager
        m_sfxManager = sceneStarter.Engine.GetSystem<AudioSystem>().GetManager<SfxManager>();
        Debug.Assert(null != m_sfxManager);
      }

      public override void OnStartGame() {
        base.OnStartGame();
      }
      public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
        base.OnReceiveEvent(ev, scope);
        if (IsActive) {
          if (m_onStopAllSfx.Matches(ev, scope)) {
            StopAllSound();
            return true;
          }
        }
        return false;
      }
      #endregion

      void StopAllSound() {
        foreach (var sfx in m_sfxManager.m_sources) {
          string soundName = sfx.GetAudioName();
          if (sfx.GetAudioName() != m_ptdSystem.GetCurrentBGM()) {
            bool filtered = false;
            if (m_isFiltered) {
              for (int i = 0; i < m_filteredSounds.Length; i++) {
                if (soundName == m_filteredSounds[i]) {
                  filtered = true;
                  break;
                }
              }
            }
            if (!filtered) {
              sfx.StopSoundImmediate();
            }
          }
        }
      }
    }
  }
}