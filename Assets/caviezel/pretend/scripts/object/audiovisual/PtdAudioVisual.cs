﻿using System.Collections.Generic;
using UnityEngine;
using Caviezel.Audio;
using Caviezel.Core;

namespace Caviezel {
    namespace Pretend {
        [DisallowMultipleComponent]
        public class PtdAudioVisual : PtdComponentActive {
            public AudioVisualEv[] m_audioVisualEvs;
            [Tooltip("play this tween(s) on start")]
            public string[] m_onStartTweenIds;

            SfxManager m_sfxManager;

            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
				base.InitComponent(sceneStarter, gameController);
                //cache the sfx manager
                m_sfxManager = sceneStarter.Engine.GetSystem<AudioSystem>().GetManager<SfxManager>();
                Debug.Assert(null != m_sfxManager);
                if (null != m_onStartTweenIds && m_onStartTweenIds.Length > 0) {
                    //play the on awake tween(s)
                    Owner.Tweener.PlayExistingTween(m_onStartTweenIds);
                }
			}            		

            public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {
                    for (int i = 0; i < m_audioVisualEvs.Length; ++i) {
                        if (m_audioVisualEvs[i].Matches(ev, scope)) {                            
                            PlayAudioVisual(m_audioVisualEvs[i]);
                            return true;
                        }
                    }
                }
                return false;
			}

			public override void OnLoaded() {
                base.OnLoaded();
                //fast forward the last tween
                string lastTweenId;
                if (Owner.ObjectData.TryGetString(SaveKeys.LastPlayedTween + ComponentId, out lastTweenId)) {
                    Owner.Tweener.PlayExistingTween(new string[] { lastTweenId });
                    Owner.Tweener.FastForward();
                }
			}

			void PlayAudioVisual(AudioVisualEv ev) {                
                HashSet<string> tweenIds = new HashSet<string>();
                //play tweens
                TweenableData tweenData = ev.m_tween;
                if (!string.IsNullOrEmpty(tweenData.m_tweenId)) {
                    if (!tweenData.m_shouldStop) {
                        tweenIds.Add(tweenData.m_tweenId);
                        //save the last tween for me
                        Owner.ObjectData.InsertPair(new KVData(SaveKeys.LastPlayedTween + ComponentId, tweenData.m_tweenId));   
                    } else {
                        Owner.Tweener.StopTween(tweenData.m_tweenId);
                        BroadcastEvent(ev.m_onDoneTweenEv);
                    }
                }
                //tell owner to play the existing tweens
                Owner.Tweener.PlayExistingTween(tweenIds, ()=>{                    
                    BroadcastEvent(ev.m_onDoneTweenEv);   
                });
                //play sounds if it's not in fast forward mode                   
                m_sfxManager.PlaySound(ev.m_sfxs);   
            }
		}
    }
}