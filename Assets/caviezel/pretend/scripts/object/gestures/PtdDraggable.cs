﻿using Caviezel.Audio;
using Caviezel.Core;
using Caviezel.UI;
using UnityEngine;

namespace Caviezel {
  namespace Pretend {
    [DisallowMultipleComponent]
    public class PtdDraggable : PtdComponentActive {
      public bool m_hasBeginEv;
      [ConditionalHide("m_hasBeginEv", true)]
      public BaseSenderEv m_beginEv;
      public bool m_hasEndEv;
      [ConditionalHide("m_hasEndEv", true)]
      public BaseSenderEv m_endEv;
      public SfxData m_beginDragSfx;
      public SfxData m_endDragSfx;
      public ResizeableImage m_tapArea;
      public bool m_parentRemains;
      public bool m_siblingRemains;
      public bool m_onlyHorizontalMove;
      public bool m_onlyVerticalMove;
      public bool m_isUsingBorder;
      [ConditionalHide("m_isUsingBorder", true)]
      public RectTransform m_border;

      protected bool m_hasDragged;

      protected RectTransform m_dragTarget;
      protected SfxManager m_sfxManager;
      IGameController m_gameController;

      #region IPtdObjectComponent
      public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
        base.InitComponent(sceneStarter, gameController);
        //resize the tap area
        m_tapArea.Resize();
        //cache the sfx manager
        m_sfxManager = sceneStarter.Engine.GetSystem<AudioSystem>().GetManager<SfxManager>();
        Debug.Assert(null != m_sfxManager);
        //set the target to owner's rect transform                                   
        m_dragTarget = Owner.RootTransform;
        //cache the gc
        m_gameController = gameController;
        //set draggable
        Owner.InitGestureHandler();
        Owner.Gestures.SetDraggable();
        Owner.Gestures.OnStartSwipeListeners += OnStartSwipe;
        Owner.Gestures.OnSwipingListeners += OnSwiping;
        Owner.Gestures.OnEndSwipeListeners += OnEndSwipe;
      }
      #endregion

      #region ISwipeListener
      public virtual void OnStartSwipe(SwipeEventData ev) {
        if (CanStartDrag()) {
          m_gameController.BeginComponentInteraction(Owner);
          //broadcast begin drag ev
          if (m_hasBeginEv) {
            BroadcastEvent(m_beginEv);
          }
          if (!m_parentRemains) {
            m_dragTarget.SetParent(m_gameController.MainRoot);
            Owner.SetParent(null);
          }
          if (!m_siblingRemains) {
            m_dragTarget.SetAsLastSibling();
          }
          Owner.IsInAction = true;
          //set the flag to true
          m_hasDragged = true;
          //play on begin drag sound
          m_sfxManager.PlaySound(m_beginDragSfx);
        }
      }

      public virtual void OnSwiping(SwipeEventData ev) {
        if (!IsActive) {
          return;
        }
        if (m_hasDragged) {
          if (m_onlyHorizontalMove) {
            ev.Delta.y = 0;
          } else if (m_onlyVerticalMove) {
            ev.Delta.x = 0;
          }

          m_dragTarget.SetScaledPos(ev.Delta);

          // force move if out of border
          if (m_isUsingBorder) {
            CheckOutOfBorder();
          }
        }
      }

      public virtual void OnEndSwipe(SwipeEventData ev) {
        if (m_hasDragged) {
          m_gameController.ProcessComponentInteraction(Owner);
          m_hasDragged = false;
          //play on end sound
          m_sfxManager.PlaySound(m_endDragSfx);
          //set flag to false
          Owner.IsInAction = false;
          //broadcast
          if (m_hasEndEv) {
            BroadcastEvent(m_endEv);
          }
        }
      }
      #endregion

      bool CanStartDrag() {
        if (!IsActive) {
          return false;
        }
        //not able to drag when the ptdobject is currently doing another action                   
        return !Owner.IsInAction;
      }

      void CheckOutOfBorder() {
        float x = m_dragTarget.X();
        float y = m_dragTarget.Y();
        float objHalfW = m_dragTarget.Width() * .5f;
        float objHalfH = m_dragTarget.Height() * .5f;


        float halfW = m_border.Width() * .5f;
        float halfH = m_border.Height() * .5f;
        float left = m_border.X() - halfW;
        float right = m_border.X() + halfW;
        float top = m_border.Y() + halfH;
        float bottom = m_border.Y() - halfH;
        if (x + objHalfW > right) {
          m_dragTarget.SetX(right - objHalfW);
        } else if (x - objHalfW < left) {
          m_dragTarget.SetX(left + objHalfW);
        } else if (y + objHalfH > top) {
          m_dragTarget.SetY(top - objHalfH);
        } else if (y - objHalfH < bottom) {
          m_dragTarget.SetY(bottom + objHalfH);
        }
      }
    }
  }
}