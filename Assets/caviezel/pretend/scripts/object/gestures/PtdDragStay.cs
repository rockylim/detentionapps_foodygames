﻿using Caviezel.UI;

namespace Caviezel {
    namespace Pretend {
        public class PtdDragStay : PtdDraggable {
            public PtdObject m_target;
            public string m_onCollidingTweenId;
            public int m_maxCount;
            public bool m_hasExceedMaxEv;
            [ConditionalHide("m_hasExceedMaxEv", true)]
            public BaseSenderEv m_onExceedMaxEv;
            public bool m_hasCounterEv;
            [ConditionalHide("m_hasCounterEv", true)]            
            public BaseSenderEv m_onIncrementedEv;
			public bool m_hasCounterWithNumberEv;
			[ConditionalHide("m_hasCounterWithNumberEv", true)]
			public BaseSenderEv m_onIncrementedWithNumberEv;
            public bool m_hasResetCounterEv;
            [ConditionalHide("m_hasResetCounterEv", true)]
            public BaseReceiverEv m_onResetCounterEv;

            int m_counter;

            public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {                   
                    if (m_onResetCounterEv.Matches(ev, scope)) {
                        m_counter = 0;
                        return true;
                    }                    
                }
                return false;
            }

            public override void OnSwiping(SwipeEventData ev) {
                base.OnSwiping(ev);
                //check collision
                if (m_counter < m_maxCount) {
                    if (Owner.RootTransform.RectAgainstRoot().Intersects(m_target.RootTransform.RectAgainstRoot())) {
                        if (!m_target.Tweener.IsTweenPlaying(m_onCollidingTweenId)) {
                            m_target.Tweener.PlayExistingTween(new string[] { m_onCollidingTweenId });
                            ++m_counter;
                            if (m_hasCounterEv) {
                                BroadcastEvent(m_onIncrementedEv);
                            }

							if (m_hasCounterWithNumberEv) {
								BroadcastEvent(new BaseSenderEv(m_onIncrementedWithNumberEv.m_senderId, m_onIncrementedWithNumberEv.m_eventId + m_counter.ToString()));
                            }

                            if (m_hasExceedMaxEv && m_counter >= m_maxCount) {
                                BroadcastEvent(m_onExceedMaxEv);
                            } 
                        }
                    }   
                }
			}
		}       
    }
}
