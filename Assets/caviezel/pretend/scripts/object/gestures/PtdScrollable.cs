﻿using UnityEngine;
using Caviezel.Core;
using Caviezel.UI;

namespace Caviezel {
    namespace Pretend {
        public class PtdScrollable : PtdComponentBase, IUpdateableComponent {
            public SwipeAxis m_axis;
            public Vector2 m_limit;
            public bool m_isChildBlock;

            ScrollableCell[] m_cells;
            CGestureHandler m_gestureHandler;
            CScrollable m_scrollable;


            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
                //init gesture
                m_gestureHandler = new CGestureHandler(gameObject);
                m_gestureHandler.SetDraggable();
                m_gestureHandler.OnStartSwipeListeners += OnBeginScroll;
                m_gestureHandler.OnSwipingListeners += OnScroll;
                m_gestureHandler.OnEndSwipeListeners += OnEndScroll;
                //init the cell(s)
                if (!m_isChildBlock) {
                    PtdObject[] objs = GetComponentsInChildren<PtdObject>(true);
                    m_cells = new ScrollableCell[objs.Length];

                    for (int i = 0; i < m_cells.Length; ++i) {
                        m_cells[i] = new ScrollableCell(this, objs[i]);
                    }
                }

                //init scrollable
                m_scrollable = new CScrollable(GetComponent<RectTransform>(), m_axis, m_limit);
            }

            public void UpdateComponent(float dt) {
                //update cell
                if (!m_isChildBlock) {
                    for (int i = 0; i < m_cells.Length; ++i) {
                        m_cells[i].UpdateCell();
                    }
                }
                //update scroll
                m_scrollable.UpdateScroll(dt);
            }

            public void OnBeginScroll(SwipeEventData ev) {
                m_scrollable.OnBeginScroll();
            }

            public void OnScroll(SwipeEventData ev) {
                m_scrollable.OnScroll(ev.Delta);
            }

            public void OnEndScroll(SwipeEventData ev) {
                m_scrollable.OnEndScroll();
            }
        }
    }
}
