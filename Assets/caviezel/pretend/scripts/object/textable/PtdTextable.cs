﻿using UnityEngine.UI;

namespace Caviezel {
    namespace Pretend {
        public class PtdTextable : PtdComponentActive {
            public TextEv[] m_textEvs;
            public Text m_text;
            public bool m_hasShowEv;
            [ConditionalHide("m_hasShowEv", true)]
            public BaseSenderEv m_onShowEv;

            public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {
                    //text ev
                    for (int i = 0; i < m_textEvs.Length; ++i) {
                        if (m_textEvs[i].Matches(ev, scope)) {
                            SetText(m_textEvs[i].m_text);
                            return true;
                        }
                    }
                }
                return false;
            }

            void SetText(string txt) {
                m_text.text = txt;
                if (m_hasShowEv) {
                    BroadcastEvent(m_onShowEv);
                }
            }
        }
    }
}
