﻿using System.Collections.Generic;
using UnityEngine;
using Caviezel.Chrono;

namespace Caviezel {
    namespace Pretend { 
        public class CAllignable {
            RectTransform m_rectTransform;
            Vector2 m_start;
            Vector2 m_end;
            Timer m_timer;

            public CAllignable(RectTransform rt, Vector2 end, float time) {
                m_rectTransform = rt;
                m_start = rt.Pos();
                m_end = end;
                m_timer = new Timer(time);
            }

            public void Arrange(float dt) {
                m_timer.UpdateTimer(dt);
                m_rectTransform.SetPos(Vector2.Lerp(m_start, m_end, m_timer.GetPercentage()));
            }
        }

        public class CAlligner {
            List<CAllignable> m_units = new List<CAllignable>();

            public CAlligner(List<RectTransform> rTransforms, float time, float spacing) {
                
                rTransforms.Sort(delegate(RectTransform lhs, RectTransform rhs) {
                    return lhs.X() < rhs.X() ? -1 : 1;
                });

                float totalW = 0f;

                for (int i = 0; i < rTransforms.Count; ++i) {
                    RectTransform rT = rTransforms[i];                                           
                    totalW += rT.Width();
                    if (i > 0 && i < rTransforms.Count) {
                        totalW += spacing;
                    }
                }

                float destX = - (totalW / 2f);

                for (int i = 0; i < rTransforms.Count; ++i) { 
                    //y remains
                    float y = GetY(rTransforms[i]);
                    destX += rTransforms[i].Width() / 2f;
                    //add to the list
                    m_units.Add(new CAllignable(rTransforms[i], new Vector2(destX, y), time));
                    destX += rTransforms[i].Width() / 2f + spacing;
                }   
            }

            public void Arrange(float dt) {                
                for (int i = 0; i < m_units.Count; ++i) {
                    m_units[i].Arrange(dt);
                }
            }

            float GetY(RectTransform rt) {
                return rt.Height() / 2f;
            }
        }

        public class CStacker {
            List<CAllignable> m_units = new List<CAllignable>();

            public CStacker(List<RectTransform> rTransforms, Vector2 startPos, int axis, int direction, float spacing, float time) {
                Vector2 dest = startPos;
                for (int i = 0; i < rTransforms.Count; ++i) {                    
                    //add
                    m_units.Add(new CAllignable(rTransforms[i], dest, time));
                    float size = rTransforms[i].Size()[axis];
                    dest[axis] += size * direction + spacing;                       
                }
            }

            public void Arrange(float dt) {
                for (int i = 0; i < m_units.Count; ++i) {
                    m_units[i].Arrange(dt);
                }
            }
        }
    }
}