﻿using UnityEngine;
using Caviezel.Core;

namespace Caviezel {
    namespace Pretend {
        public class PtdPuzzleable : PtdComponentActive, IUpdateableComponent {
            public PtdComponentBase[] m_puzzleables;
            public PuzzleableEv m_onSolvedEv;

            IPuzzleSolveCondition[] m_puzzleConditions;
            bool m_hasSolved = false;

			public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
                //init solve conditions
                m_puzzleConditions = new IPuzzleSolveCondition[m_puzzleables.Length];
                for (int i = 0; i < m_puzzleables.Length; ++i) {
                    IPuzzleSolveCondition solveCondition = m_puzzleables[i] as IPuzzleSolveCondition;
                    Debug.Assert(null != solveCondition, string.Format("component with name = {0} doesnt implement IPuzzleSolveCondition!", m_puzzleables[i].name));
                    m_puzzleConditions[i] = solveCondition;
                }
			}

			public void UpdateComponent(float dt) {
                if (!m_hasSolved) {
                    bool hasAllSolved = true;
                    for (int i = 0; i < m_puzzleConditions.Length; ++i) {
                        if (!m_puzzleConditions[i].HasSolved) {
                            hasAllSolved = false;
                            break;
                        }
                    }
                    if (hasAllSolved) {
                        m_hasSolved = true;
                        BroadcastEvent(m_onSolvedEv);
                    }
                }
            }
        }       
    }
}
