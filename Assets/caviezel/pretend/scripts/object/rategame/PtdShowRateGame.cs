﻿using UnityEngine;
using Caviezel.Core;
using RateGameSystem;

namespace Caviezel {
    namespace Pretend {
        public class PtdShowRateGame : PtdComponentActive {
            public BaseReceiverEv[] m_shouldShowRateGame;
            public bool m_isCheckCounter;

            RateGameManager m_system;

            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
                //cache achievement system
                m_system = sceneStarter.Engine.GetSystem<RateGameManager>();
                Debug.Assert(null != m_system);
            }

            public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {
                    for (int i = 0; i < m_shouldShowRateGame.Length; ++i) {
                        if (m_shouldShowRateGame[i].Matches(ev, scope)) {
                            m_system.Show(m_isCheckCounter);
                            return true;
                        }
                    }
                }
                return false;
            }
        }

    }
}
