﻿using UnityEngine;
using Caviezel.Core;
using RateGameSystem;

namespace Caviezel {
    namespace Pretend {
        public class PtdRateGameCounter : PtdComponentActive {
            public BaseReceiverEv[] m_onAddCount;
            public BaseSenderEv m_onExceedMaxEv;


            RateGameManager m_system;

            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
                //cache achievement system
                m_system = sceneStarter.Engine.GetSystem<RateGameManager>();
                Debug.Assert(null != m_system);
            }

            public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {
                    for (int i = 0; i < m_onAddCount.Length; ++i) {
                        if (m_onAddCount[i].Matches(ev, scope)) {
                            m_system.AddCount();
                            if (m_system.IsExceedCounter()) {
                                BroadcastEvent(m_onExceedMaxEv);
                            }
                            return true;
                        }
                    }
                }
                return false;
            }
        }

    }
}
