﻿using System.Collections.Generic;
using UnityEngine;
using Caviezel.Core;

namespace Caviezel {
    namespace Pretend {        
        public class PtdCollider : PtdComponentActive, ICollider, IDataProvider<ParentAreaData> {            
            public List<ColliderData> m_colliders;
            public bool m_hasRuntimeCollider;
            [ConditionalHide("m_hasRuntimeCollider", true)]
            public RuntimeColliderData[] m_runtimeColliders;
            public CollisionData[] m_collisionTriggers;

            #region IDataProvider
            public ParentAreaData ProvideData(SaveObjectData data) {
                string parentId;
                if (data.TryGetString(SaveKeys.Parent, out parentId)) {
                    if (Owner.ObjectId.IsEqual(parentId)) {
                        string areaId;
                        if (data.TryGetString(SaveKeys.ColliderArea, out areaId)) {
                            for (int i = 0; i < Colliders.Count; ++i) {
                                if (Colliders[i].m_id.IsEqual(areaId)) {
                                    return new ParentAreaData(Colliders[i].m_area);
                                }
                            }
                        }
                    }
                }
                return null;
            }
            #endregion

            #region IPtdCollideable
            public RectTransform RootRectTransform { get; private set; }

            public IList<ColliderData> Colliders { get { return m_colliders; } }

            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
                //add runtime colliders if necessary
                if (m_hasRuntimeCollider) {
                    for (int i = 0; i < m_runtimeColliders.Length; ++i) {
                        m_colliders.Add(m_runtimeColliders[i].Generate());
                    }
                }               
                //cache the root transform
                RootRectTransform = GetComponent<RectTransform>();
            }

            public bool ShouldCollide(OnCollideData collisionData) {                
                for (int i = 0; i < m_collisionTriggers.Length; ++i) {                    
                    if (m_collisionTriggers[i].Matches(collisionData)) {
                        CollisionSenderEv collisionEv = new CollisionSenderEv(m_collisionTriggers[i].m_senderEv, collisionData);
                        //other coll will receive the event as internal scope
                        PtdObject otherObj = (collisionData.m_collider.Owner.GetInstanceID() == Owner.GetInstanceID()) ? collisionData.m_otherCollider.Owner : collisionData.m_collider.Owner;
                        otherObj.OnReceiveEvent(collisionEv, EventScope.Internal);
                        //broadcast collision ev
                        BroadcastEvent(collisionEv);
                        //once only
                        return true;
                    }
                }
                return false;
            }
            #endregion
        }       
    }
}
