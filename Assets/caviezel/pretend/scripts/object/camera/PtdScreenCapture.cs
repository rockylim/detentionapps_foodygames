﻿using UnityEngine;
using Caviezel.Native;

namespace Caviezel {
    namespace Pretend {
        public class PtdScreenCapture : PtdComponentActive {
            public ScreenCaptureEv[] m_captureEvs;
            [Tooltip("file name will start with this prefix")]
            public string m_fileName;

			public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
                base.OnReceiveEvent(ev, scope);
                if (IsActive) {
                    for (int i = 0; i < m_captureEvs.Length; ++i) {
                        if (m_captureEvs[i].Matches(ev, scope)) {
                            Capture();
                            return true;
                        }
                    }
                }
                return false;
			}

            void Capture() {
                #if !UNITY_EDITOR
                ICamera cam = null;            
                #if UNITY_IOS
                cam = new CIosCamera();
                #endif
                #if UNITY_ANDROID
                cam = new CAndroidCamera(m_fileName);
                #endif
                if (null != cam) {
                    StartCoroutine(cam.CaptureScreen());   
                }
                #endif
            }                     
		}       
    }
}