﻿using Caviezel.Chrono;
using UnityEngine;
using UnityEngine.UI;

namespace Caviezel {
  namespace Pretend {
    public class PtdParticle : MonoBehaviour, IParticle {
      protected RectTransform m_area;
      protected RectTransform m_rectTransform;
      protected Image m_image;
      protected System.Random m_rand;
      protected Timer m_timer;
      protected PtdParticleController m_controller;
      protected bool m_isPlaying;
      protected bool m_isShowing;
      protected bool m_isHiding;
      protected float m_speedX;
      protected float m_speedY;
      protected float m_speedR;
      protected float m_alphaInc;

      // customization area
      protected PtdParticleController.ParticleRangeCustomization m_showDelay;
      protected PtdParticleController.ParticleRangeCustomization m_speedXRange;
      protected PtdParticleController.ParticleRangeCustomization m_speedYRange;



      public virtual void InitParticle(PtdParticleController controller, RectTransform area) {
        m_controller = controller;
        m_area = area;
        m_rectTransform = GetComponent<RectTransform>();
        m_image = GetComponent<Image>();
        m_rand = MathExtensions.GetRandom();
        m_speedX = 0;
        m_speedY = 0;
        m_speedY = 0;
        m_isPlaying = false;
        m_isShowing = false;
        m_isHiding = false;
        gameObject.SetActive(false);
        ResetParticle();
      }

      public virtual void UpdateParticle(float dt) {
        if (null != m_timer) {
          UpdateTimer(dt);
        }
        if (m_isShowing) {
          UpdateShow(dt);
        }
        if (m_isHiding) {
          UpdateHide(dt);
        }
        if (m_isPlaying) {
          UpdateMove(dt);
        }
      }
      public virtual void UpdateMove(float dt) { }
      public virtual void UpdateShow(float dt) {
        m_image.SetAlpha(m_image.color.a + (m_alphaInc * dt));
        if (m_image.color.a >= 1f) {
          m_image.SetAlpha(1f);
          m_isShowing = false;
        }
      }
      public virtual void UpdateHide(float dt) {
        m_image.SetAlpha(m_image.color.a - (m_alphaInc * dt));
        if (m_image.color.a <= 0f) {
          m_image.SetAlpha(0f);
          m_isHiding = false;
          m_isPlaying = false;
          if (m_controller.m_isLoop) {
            OnInitTimerShow();
          } else {
            OnDone();
          }
        }
      }

      public virtual void UpdateTimer(float dt) {
        if (!m_timer.UpdateTimer(dt)) {
          if (!m_isPlaying) {
            m_timer = null;
            OnShow();
          } else {
            m_timer = null;
            OnHide();
          }
        }
      }
      public virtual void OnHide() {
        m_isHiding = true;
      }

      public virtual void OnInitTimerShow() {
        ResetParticle();
        m_image.SetAlpha(0);
      }

      public virtual void OnShow() {
        gameObject.SetActive(true);
        m_isShowing = true;
        m_isPlaying = true;
      }
      public virtual void OnDone() {
        gameObject.SetActive(false);
        m_isPlaying = false;
        m_controller.OnParticleStop();
      }
      public virtual void ResetParticle() { }

      protected void SetShowDelay(PtdParticleController.ParticleRangeCustomization defaultRange) {
        m_showDelay = defaultRange;
        if (!Mathf.Approximately(m_controller.m_showDelay.min, 0) || !Mathf.Approximately(m_controller.m_showDelay.max, 0)) {
          m_showDelay = m_controller.m_showDelay;
        }
      }

      protected void SetSpeed(PtdParticleController.ParticleRangeCustomization defaultRangeSpeedX, PtdParticleController.ParticleRangeCustomization defaultRangeSpeedY) {
        m_speedXRange = defaultRangeSpeedX;
        m_speedYRange = defaultRangeSpeedY;
        if (!Mathf.Approximately(m_controller.m_speedXRange.min, 0) || !Mathf.Approximately(m_controller.m_speedXRange.max, 0)) {
          m_speedXRange = m_controller.m_speedXRange;
        }
        if (!Mathf.Approximately(m_controller.m_speedYRange.min, 0) || !Mathf.Approximately(m_controller.m_speedYRange.max, 0)) {
          m_speedYRange = m_controller.m_speedYRange;
        }
      }
    }
  }
}

