﻿using UnityEngine;
using Caviezel.Chrono;
using Caviezel.Tween;
using System.Collections.Generic;

namespace Caviezel {
    namespace Pretend {
        public class PtdSpread : PtdParticle {
            CTweenMove m_tweenMove;
            float m_targetX;
            float m_targetY;
            List<ITween> m_tweens = new List<ITween>();

            public override void InitParticle(PtdParticleController controller, RectTransform area) {
                base.InitParticle(controller, area);
                m_alphaInc = 20f;
                m_area = area;
                SetShowDelay(new PtdParticleController.ParticleRangeCustomization(0, 200));
                SetSpeed(new PtdParticleController.ParticleRangeCustomization(100, 250), new PtdParticleController.ParticleRangeCustomization(0, 0));
            }

            public override void UpdateMove(float dt) {
                base.UpdateMove(dt);
                for (int i = 0; i < m_tweens.Count; ++i) {
                    m_tweens[i].UpdateTween(dt);
                }
            }

            public override void OnInitTimerShow() {
                base.OnInitTimerShow();
                m_timer = new Timer(m_rand.Next(m_showDelay.min, m_showDelay.max) / 100f);
            }

            public override void OnShow() {
                base.OnShow();
            }

            public override void OnDone() {
                for (int i = 0; i < m_tweens.Count; ++i) {
                    m_tweens[i].Stop();
                }
                m_tweens = new List<ITween>();
                base.OnDone();
            }

            public override void ResetParticle() {
                base.ResetParticle();
                m_rectTransform.SetPos(new Vector2(0f, 0f));
                int halfHeight = (int)(m_area.Height() * .5f);
                int halfWidth = (int)(m_area.Width() * .5f);

                int randPosition = m_rand.Next(1, 3);
                if (randPosition == 1) {
                    m_targetX = GetRandomTarget(halfHeight + 50, halfHeight + 100);
                    m_targetY = GetRandomTarget(0, halfWidth + 100);
                } else {
                    m_targetY = GetRandomTarget(halfWidth + 50, halfWidth + 100);
                    m_targetX = GetRandomTarget(0, halfHeight + 100);
                }

                float randomScale = m_rand.Next(20, 100) / 100f;
                m_rectTransform.SetScale(new Vector2(randomScale, randomScale));

                float randomRotation = m_rand.Next(0, 360);
                m_rectTransform.SetRotation(randomRotation);

                m_image.SetAlpha(0f);
                InitTween();
            }

            float GetRandomTarget(int range1, int range2) {
                float target = m_rand.Next(range1, range2);

                int randomTarget = m_rand.Next(1, 3);
                if (randomTarget == 1) {
                    target = -target;
                }

                return target;
            }

            void InitTween() {
                float time = m_rand.Next(m_speedXRange.min, m_speedXRange.max) / 100f;
                m_tweens = new List<ITween>();
                m_tweenMove = new CTweenMove(
                    TargetType.RectTransform
                    , gameObject
                    , new MoveData(new Vector2(m_targetX, m_targetY), time, TweenType.To, Easing.Linear, 0f));
                m_tweenMove.Start().SetCompleteCallback(() => {
                    OnHide();
                });
                m_tweens.Add(m_tweenMove);
            }
        }
    }
}


