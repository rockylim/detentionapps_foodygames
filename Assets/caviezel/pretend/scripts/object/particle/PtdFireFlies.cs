﻿using System.Collections.Generic;
using Caviezel.Chrono;
using Caviezel.Tween;
using UnityEngine;

namespace Caviezel {
  namespace Pretend {
    public class PtdFireFlies : PtdParticle {
      CTweenMove m_tweenMove;
      CTweenFade m_tweenFade;
      float m_targetX;
      float m_targetY;
      Timer m_fadehideTimer;
      Timer m_fadeStateTimer;
      bool m_isBright;
      List<ITween> m_tweens = new List<ITween>();

      public override void InitParticle(PtdParticleController controller, RectTransform area) {
        base.InitParticle(controller, area);
        m_area = area;
        SetShowDelay(new PtdParticleController.ParticleRangeCustomization(0, 500));
        SetSpeed(new PtdParticleController.ParticleRangeCustomization(1600, 2000), new PtdParticleController.ParticleRangeCustomization(0, 0));
      }

      public override void UpdateMove(float dt) {
        base.UpdateMove(dt);
        Debug.Log("MO");
        for (int i = 0; i < m_tweens.Count; ++i) {
          m_tweens[i].UpdateTween(dt);
        }

        if (m_tweenFade != null) {
          m_tweenFade.UpdateTween(dt);
        }

        if ((m_fadeStateTimer != null) && (!m_fadeStateTimer.UpdateTimer(dt))) {
          m_fadeStateTimer = null;
          m_isBright = !m_isBright;
          float fade = m_isBright ? ((m_rand.Next(5, 8)) * .1f) : ((m_rand.Next(0, 2)) * .1f);

          float fadeTime = m_rand.Next(10, 21) * .1f;
          m_tweenFade = new CTweenFade(gameObject, fade, fadeTime, TargetType.Image);
          m_tweenFade.Start().SetCompleteCallback(() => {
            m_fadeStateTimer = new Timer(m_rand.Next(2, 4));
            m_tweenFade = null;
          });
        }

        if ((m_fadehideTimer != null) && (!m_fadehideTimer.UpdateTimer(dt))) {
          m_fadehideTimer = null;
          if (m_tweenFade != null) {
            m_tweenFade.Stop();
            m_tweenFade = null;
          }
          m_fadeStateTimer = null;
          InitFadeHide();
        }

      }

      public override void OnInitTimerShow() {
        base.OnInitTimerShow();
        m_timer = new Timer(m_rand.Next(m_showDelay.min, m_showDelay.max) / 100f);
      }

      public override void OnShow() {
        base.OnShow();
        m_isBright = true;
        m_fadeStateTimer = new Timer(m_rand.Next(1, 3));
      }

      public override void OnDone() {
        for (int i = 0; i < m_tweens.Count; ++i) {
          m_tweens[i].Stop();
        }
        m_tweens = new List<ITween>();
        base.OnDone();
      }

      public override void ResetParticle() {
        base.ResetParticle();
        int halfHeight = (int)(m_area.Height() * .5f);
        int halfWidth = (int)(m_area.Width() * .5f);

        float m_posX = GetRandomTarget(0, halfWidth);
        float m_posY = GetRandomTarget(0, halfHeight);
        m_targetX = GetRandomTarget(0, halfWidth);
        m_targetY = GetRandomTarget(0, halfHeight);
        m_rectTransform.SetPos(new Vector2(m_posX, m_posY));

        float randomScale = m_rand.Next(20, 100) / 100f;
        m_rectTransform.SetScale(new Vector2(randomScale, randomScale));

        float randomRotation = m_rand.Next(0, 360);
        m_rectTransform.SetRotation(randomRotation);

        m_alphaInc = m_rand.Next(1, 2);

        m_image.SetAlpha(0f);
        InitTween();
      }

      float GetRandomTarget(int range1, int range2) {
        float target = m_rand.Next(range1, range2);

        int randomTarget = m_rand.Next(1, 3);
        if (randomTarget == 1) {
          target = -target;
        }

        return target;
      }

      void InitTween() {
        float time = m_rand.Next(m_speedXRange.min, m_speedXRange.max) / 100f;
        m_fadehideTimer = new Timer(time - 8);
        m_tweens = new List<ITween>();
        m_tweenMove = new CTweenMove(
            TargetType.RectTransform
            , gameObject
            , new MoveData(new Vector2(m_targetX, m_targetY), time, TweenType.To, Easing.Linear, 0f));
        m_tweenMove.Start();
        m_tweens.Add(m_tweenMove);
      }

      void InitFadeHide() {

        m_tweenFade = new CTweenFade(gameObject, 0f, 0.5f, TargetType.Image);
        m_tweenFade.Start().SetCompleteCallback(() => {
          m_tweenFade = null;
          OnHide();
        });
      }
    }
  }
}


