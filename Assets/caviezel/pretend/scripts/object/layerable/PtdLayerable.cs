﻿using Caviezel.Core;
using UnityEngine;

namespace Caviezel {
    namespace Pretend {
        [DisallowMultipleComponent]
        public class PtdLayerable : PtdComponentBase, ILayerable {
            public RectTransform m_layerableArea;

            public float Pivot {
                get {return Owner.RootTransform.Y() - LayerArea.Height() / 2f;}
            }

            public RectTransform LayerArea {
                get; private set;
            }

            public void SortLayer() {
                Owner.RootTransform.SetAsLastSibling();
            }

            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
                //set the layer area
                LayerArea = m_layerableArea;
            }
        }
    }
}
