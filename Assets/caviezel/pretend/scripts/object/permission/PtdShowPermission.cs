﻿using System;
using Caviezel.AppPermission;
using Caviezel.Core;
using UnityEngine;

namespace Caviezel {
  namespace Pretend {
    public class PtdShowPermission : PtdComponentActive {
      [Serializable]
      public struct PermissionData {
        public BaseReceiverEv m_onShow;
        public string m_permissionName;
      }
      [Header("ANDROID")]
      public PermissionData[] m_shouldShowAndroidPermission;


      [Header("IOS")]
      public PermissionData[] m_shouldShowIOSPermission;


      AppPermissionController m_system;

      public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
        base.InitComponent(sceneStarter, gameController);
        //cache achievement system
        m_system = sceneStarter.Engine.GetSystem<AppPermissionController>();
        Debug.Assert(null != m_system);
      }

      public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
        base.OnReceiveEvent(ev, scope);
        if (IsActive) {
#if UNITY_IOS
          for (int i = 0; i < m_shouldShowIOSPermission.Length; ++i) {
            if (m_shouldShowIOSPermission[i].m_onShow.Matches(ev, scope)) {
              m_system.ShowPermission(m_shouldShowIOSPermission[i].m_permissionName);
              return true;
            }
          }
#endif
#if UNITY_ANDROID
          for (int i = 0; i < m_shouldShowAndroidPermission.Length; ++i) {
            if (m_shouldShowAndroidPermission[i].m_onShow.Matches(ev, scope)) {
              m_system.ShowPermission(m_shouldShowAndroidPermission[i].m_permissionName);
              return true;
            }
          }
#endif
        }
        return false;
      }
    }

  }
}
