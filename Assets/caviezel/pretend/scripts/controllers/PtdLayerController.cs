﻿using System.Collections.Generic;
using UnityEngine;

namespace Caviezel {
    namespace Pretend {
        public class PtdLayerController : IComponentController {
            RectTransform m_layerGroup;
            List<ILayerable> m_layerables = new List<ILayerable>();
            IComponent m_processedComponent;

            public PtdLayerController(RectTransform layerGroup) {
                m_layerGroup = layerGroup;
            }

            #region IComponentController
            public void OnProcessedComponent(IComponent comp) {
                m_processedComponent = comp;
            }

            public void RetainComponent(IComponent comp) {
                ILayerable layerable = comp as ILayerable;
                if (null != layerable) {                    
                    m_layerables.Add(layerable);
                }
            }

            public void BeginInteraction(PtdObject obj) { }

            public void ProcessInteraction(PtdObject obj) {}

            public void OnStartController() {}

            public void UpdateController(float dt) {
                if (null != m_processedComponent && !m_processedComponent.Owner.IsInAction) {
                    ILayerable layerable = m_processedComponent.Owner.ComponentSystem.GetComponent<ILayerable>();
                    if (null != layerable && null == layerable.Owner.Parent) {                        
                        Sort(layerable);    
                    }
                    m_processedComponent = null;
                }
            }

            public void OnStartGame() {
                //add the layerable to layer group
                for (int i = 0; i < m_layerables.Count; ++i) {                    
                    if (null == m_layerables[i].Owner.Parent) {
                        m_layerables[i].Owner.RootTransform.SetParent(m_layerGroup, false);
                    }
                }
                //sort
                SortExisting();
            }

            public void OnExitGame() { }
            #endregion

            void Sort(ILayerable layerable) {
                layerable.Owner.RootTransform.SetParent(m_layerGroup, false);
                SortExisting();
            }

            void SortExisting() {
                m_layerables.Sort(delegate (ILayerable lhs, ILayerable rhs) {
                    return (lhs.Pivot < rhs.Pivot) ? -1 : 1;
                });
                //iterate over and sort the layer
                for (int i = m_layerables.Count - 1; i >= 0; --i) {
                    if (null == m_layerables[i].Owner.Parent) {
                        m_layerables[i].SortLayer();
                    }
                }
            }
        }
    }
}
