﻿using UnityEngine;

namespace Caviezel {
    namespace Pretend {
        public interface IGameController {
            RectTransform MainRoot { get; }
            void RetainComponent(IComponent comp);
            void OnComponentProcessed(IComponentController processor, IComponent comp);
            void BeginComponentInteraction(PtdObject obj);
            void ProcessComponentInteraction(PtdObject obj);
            void BroadcastEvent(PtdObject obj, BaseSenderEv ev);
            void ToScene(string sceneId);
            PtdObject GetObjectById(string id);
            T GetProviderData<T>(SaveObjectData componentData) where T : class;
        }

        public interface IComponentController {
            void BeginInteraction(PtdObject obj);
            void ProcessInteraction(PtdObject obj); //called when there's an interaction for this component
            void RetainComponent(IComponent comp); //called the first time the game starts to be retained by the controllers accordingly
            void OnProcessedComponent(IComponent comp); //called when it's just being processed by the other controller
            void UpdateController(float dt); //updating
            void OnStartGame();
            void OnExitGame();
        }
    }
}
