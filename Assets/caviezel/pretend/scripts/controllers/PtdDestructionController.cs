﻿using System.Collections.Generic;

namespace Caviezel {
    namespace Pretend {
        public class PtdDestructionController : IComponentController {
            HashSet<IRespawnable> m_destroyables = new HashSet<IRespawnable>();
            IGameController m_gameController;

            public PtdDestructionController(IGameController gc) {
                m_gameController = gc;
            }

            public void OnProcessedComponent(IComponent comp) {                
            }

            public void RetainComponent(IComponent comp) {
                IRespawnable destroyable = comp as IRespawnable;
                if (null != destroyable) {
                    m_destroyables.Add(destroyable);
                }
            }

            public void BeginInteraction(PtdObject obj) { }

            public void ProcessInteraction(PtdObject obj) {}

            public void UpdateController(float dt) {
                foreach (IRespawnable destroyable in m_destroyables) {
                    PtdObject owner = destroyable.Owner;
                    //process only when the owner is not doing any action and should be destroyed
                    if (!owner.IsInAction && owner.IsDestroyed) {
                        if (!destroyable.HasRespawned) {
                            destroyable.HasRespawned = true;
                            //reset all the components' owner on respawn here
                            foreach (IComponent component in owner.ComponentSystem.Components) {
                                component.Reinit();
                            }
                        } else {
                            if (!destroyable.Respawn(dt)) {                                
                                //set the flags accordingly
                                destroyable.HasRespawned = false;
                                owner.IsDestroyed = false;
                                //tell the other controllers
                                m_gameController.OnComponentProcessed(this, destroyable);
                            }   
                        }
                    }
                }
            }

            public void OnStartGame() { }

            public void OnExitGame() { }
        }
    }
}
