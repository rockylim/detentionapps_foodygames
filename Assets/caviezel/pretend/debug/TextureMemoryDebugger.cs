﻿using UnityEngine;
using UnityEngine.Profiling;
using Caviezel.Core;

namespace Caviezel {
    namespace Pretend {
        public class TextureMemoryDebugger : PtdComponentActive, IUpdateableComponent {

            int frameCtr = 0;

            public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
                base.InitComponent(sceneStarter, gameController);
            }

            public override void OnStartGame() {
                base.OnStartGame();
            }

            public void UpdateComponent(float dt) {
#if DEVELOPMENT_BUILD || UNITY_EDITOR
                frameCtr++;

                if (frameCtr == 100) {
                    var textures = Resources.FindObjectsOfTypeAll(typeof(Texture));
                    float totalTextureMemorySize = 0;
                    foreach (Texture t in textures) {
                        float memoryUsed = Profiler.GetRuntimeMemorySizeLong(t) / 1000000f;
                        Debug.Log("**** Texture object " + t.name + " using: " + memoryUsed);
                        totalTextureMemorySize += memoryUsed;
                    }
                    Debug.Log("****  total  : " + totalTextureMemorySize);
                }
#endif
            }
        }
    }
}