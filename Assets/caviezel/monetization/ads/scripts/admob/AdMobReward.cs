﻿using System.Collections.Generic;
using GoogleMobileAds.Api;

namespace Caviezel {
  namespace Monetization {
    public class AdMobReward : IReward {
      IRewardAdListener m_listener;
      private RewardedAd rewardedAd;

      #region IReward
      public int Order { get; private set; }

      public bool ShowReward(IRewardAdListener listener) {
        m_listener = listener;
        if (rewardedAd.IsLoaded()) {
          rewardedAd.Show();
          return true;
        }
        return false;
      }
      #endregion

      public AdMobReward(RewardAdmobData admobData) {
        Order = admobData.m_showOrder;
        rewardedAd = new RewardedAd(admobData.UnitId);
        rewardedAd.OnUserEarnedReward += (sender, e) => {
          m_listener.OnRewarded();
        };
        rewardedAd.OnAdClosed += (sender, e) => {
          LoadAd();
        };
        LoadAd();
      }

      void LoadAd() {
        /*
        List<string> deviceIds = new List<string>();
        deviceIds.Add("6802DCD837792ABF057BCF4764051F5F");

        RequestConfiguration requestConfiguration = new RequestConfiguration.Builder()
        .SetTestDeviceIds(deviceIds)
        .SetTagForChildDirectedTreatment(TagForChildDirectedTreatment.True)
        .build();
        MobileAds.SetRequestConfiguration(requestConfiguration);
        */

        // Create an empty ad request.        
        AdRequest request = new AdRequest.Builder().Build();
        //load video
        rewardedAd.LoadAd(request);
      }
    }
  }
}
