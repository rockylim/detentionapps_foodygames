using System.Collections.Generic;
using GoogleMobileAds.Api;
using UnityEngine;

namespace Caviezel {
  namespace Monetization {
    public class AdMobInterstitial : IInterstitial {
      string m_unitId;
      InterstitialAd m_interstitial;

      #region IInterstitial
      public int Order { get; private set; }

      public bool ShowInterstitial() {
        Debug.Log("****** try show admob interstitial ads : " + m_unitId);
        if (m_interstitial.IsLoaded()) {
          Debug.Log("show admob ads");
          m_interstitial.Show();
          return true;
        }
        ReloadAd();
        return false;
      }
      #endregion

      public AdMobInterstitial(InterstitialAdmobData data) {
        m_unitId = data.UnitId;
        Order = data.m_showOrder;
        // load ad
        ReloadAd();
      }

      void InitInternal() {
        m_interstitial = new InterstitialAd(m_unitId);
        m_interstitial.OnAdClosed += (object sender, System.EventArgs e) => {
          ReloadAd();
        };
      }

      void ReloadAd() {
        Debug.Log("load admob ads");
#if UNITY_ANDROID
        if (null == m_interstitial) {
          InitInternal();
        }
#elif UNITY_IPHONE
                // re init intestitial on ios because it's a one time object
                InitInternal();
#endif
        /*
        List<string> deviceIds = new List<string>();
        deviceIds.Add("6802DCD837792ABF057BCF4764051F5F");

        RequestConfiguration requestConfiguration = new RequestConfiguration.Builder()
        .SetTestDeviceIds(deviceIds)
        .SetTagForChildDirectedTreatment(TagForChildDirectedTreatment.True)
        .build();
        MobileAds.SetRequestConfiguration(requestConfiguration);
        */


        // Create an empty ad request.        
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        m_interstitial.LoadAd(request);
      }
    }
  }
}
