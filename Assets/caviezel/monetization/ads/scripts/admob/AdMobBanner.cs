﻿using System.Collections.Generic;
using GoogleMobileAds.Api;
using GoogleMobileAds.Common;

namespace Caviezel {
  namespace Monetization {
    public class AdMobBanner : IBanner {
      BannerView m_banner;
      string m_unitId;

      public AdMobBanner(BannerAdmobData data) {
        //init app id
        m_unitId = data.UnitId;
        MobileAds.Initialize(OnInit);
      }

      public void Show(bool flag) {
        m_banner.Destroy();
      }

      void OnInit(InitializationStatus initstatus) {
        // Callbacks from GoogleMobileAds are not guaranteed to be called on
        // main thread.
        // In this example we use MobileAdsEventExecutor to schedule these calls on
        // the next Update() loop.
        MobileAdsEventExecutor.ExecuteInUpdate(() => {

          /*
          List<string> deviceIds = new List<string>();
          deviceIds.Add("6802DCD837792ABF057BCF4764051F5F");

          RequestConfiguration requestConfiguration = new RequestConfiguration.Builder()
          .SetTestDeviceIds(deviceIds)
          .SetTagForChildDirectedTreatment(TagForChildDirectedTreatment.True)
          .build();
          MobileAds.SetRequestConfiguration(requestConfiguration);
          */

          //load banner
          AdRequest request = new AdRequest.Builder().Build();
          //AdRequest request = new AdRequest.Builder().AddTestDevice("6802DCD837792ABF057BCF4764051F5F").Build();
          m_banner = new BannerView(m_unitId, AdSize.SmartBanner, AdPosition.Top);
          m_banner.OnAdLoaded += (sender, args) => {
            m_banner.Show();
          };
          m_banner.LoadAd(request);
        });
      }

    }
  }
}
