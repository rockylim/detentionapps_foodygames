﻿using System;

namespace Caviezel {
  namespace Monetization {
    [Serializable]
    public struct BannerAdmobData {
      public string m_tagName;
      public string m_unitIdIOS;
      public string m_unitIdAndroid;

      public string UnitId {
        get {
#if UNITY_ANDROID
          return m_unitIdAndroid;
#elif UNITY_IPHONE
                    return m_unitIdIOS;
#endif
        }
      }
    }

    [Serializable]
    public struct InterstitialAdmobData {
      public string m_unitIdIOS;
      public string m_unitIdAndroid;
      public int m_showOrder;
      public bool m_shouldLoad;

      public string UnitId {
        get {
#if UNITY_ANDROID
          return m_unitIdAndroid;
#elif UNITY_IPHONE
                    return m_unitIdIOS;
#endif
        }
      }
    }

    [Serializable]
    public struct RewardAdmobData {
      public string m_unitIdIOS;
      public string m_unitIdAndroid;
      public int m_showOrder;
      public bool m_shouldLoad;

      public string UnitId {
        get {
#if UNITY_ANDROID
          return m_unitIdAndroid;
#elif UNITY_IPHONE
                    return m_unitIdIOS;
#endif
        }
      }
    }
  }
}