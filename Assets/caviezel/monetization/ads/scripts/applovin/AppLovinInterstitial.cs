﻿using UnityEngine;

namespace Caviezel {
    namespace Monetization {
        public class AppLovinInterstitial : IInterstitial {
            #region IInterstitial
            public int Order { get; private set; }

            public bool ShowInterstitial() {
                Debug.Log("show applovin ads intertitial");
#if !UNITY_EDITOR
                AppLovin.ShowInterstitial();
#endif
                return true;
            }

            public void StartInterstitial(AdInterstitialManager manager) { }
            #endregion

            public AppLovinInterstitial(InterstitialAppLovinData data) {
                Debug.Log("init applovin");
                Order = data.m_showOrder;
#if !UNITY_EDITOR
                AppLovin.SetSdkKey(data.UnitId);
                AppLovin.InitializeSdk();
#endif
            }
        }
    }
}
