﻿using System;

namespace Caviezel {
    namespace Monetization {
        [Serializable]
        public struct InterstitialAppLovinData {
            public string m_unitId;
            public int m_showOrder;
            public bool m_shouldLoad;

            public string UnitId {
                get { return m_unitId; }
            }
        }
    }
}