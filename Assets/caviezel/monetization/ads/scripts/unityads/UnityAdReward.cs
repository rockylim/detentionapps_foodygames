﻿using UnityEngine.Advertisements;

namespace Caviezel {
  namespace Monetization {
    public class UnityAdReward : IReward, IUnityAdsInitializationListener, IUnityAdsLoadListener, IUnityAdsShowListener {

      public int Order { get; private set; }
      bool m_ready;
      IRewardAdListener m_listener;
      const string m_addUnit = "rewardedVideo";

      public UnityAdReward(UnityAdData adData) {
        Order = adData.m_showOrder;
        if (!Advertisement.isInitialized) {
          UnityEngine.Debug.Log("unity ads : init rewarded ads");

          bool isTestMode = false;
#if UNITY_EDITOR
          isTestMode = true;
#endif
          Advertisement.Initialize(adData.GameId, isTestMode);
        }
      }


      #region IReward
      public bool ShowReward(IRewardAdListener listener) {
        UnityEngine.Debug.Log("unity ads : try show unity rewarded ads");
        m_listener = listener;
        ShowOptions options = new ShowOptions();
#if UNITY_EDITOR
        m_ready = true;
#endif
        if (m_ready) {
          Advertisement.Show(m_addUnit, options);
          return true;
        }
        LoadAd();
        return false;
      }
      #endregion

      #region IUnityAdsInitializationListener
      public void OnInitializationComplete() {
        UnityEngine.Debug.Log("unity ads : init rewarded ads complete");
        LoadAd();
      }

      public void OnInitializationFailed(UnityAdsInitializationError error, string message) {
        UnityEngine.Debug.Log("unity ads : unity rewarded ads init failed " + message);
      }
      #endregion


      #region IUnityAdsLoadListener
      public void OnUnityAdsAdLoaded(string placementId) {
        if (m_addUnit.IsEqual(placementId)) {
          m_ready = true;
        }
      }

      public void OnUnityAdsFailedToLoad(string placementId, UnityAdsLoadError error, string message) {
        UnityEngine.Debug.Log("unity ads : rewarded failed to load " + message);
      }
      #endregion


      #region IUnityAdsShowListener
      public void OnUnityAdsShowClick(string placementId) { }
      public void OnUnityAdsShowComplete(string placementId, UnityAdsShowCompletionState showCompletionState) {
        if (placementId.Equals(m_addUnit)) {
          if (showCompletionState.Equals(UnityAdsShowCompletionState.COMPLETED)) {
            UnityEngine.Debug.Log("unity ads Rewarded Ad Completed");
            if (m_listener != null) {
              m_listener.OnRewarded();
              m_listener = null;
            }
          }
        }
      }
      public void OnUnityAdsShowFailure(string placementId, UnityAdsShowError error, string message) {
        UnityEngine.Debug.Log($"unity ads : Error showing rewarded Ad Unit {placementId}: {error.ToString()} - {message}");
        LoadAd();
      }

      public void OnUnityAdsShowStart(string placementId) {
        LoadAd();
      }
      #endregion

      void LoadAd() {
        UnityEngine.Debug.Log("unity ads : try to load rewarded ads");
        if (m_ready) {
          UnityEngine.Debug.Log("unity ads : rewarded ads is alreay loaded");
          return;
        }
        m_ready = false;
        Advertisement.Load(m_addUnit, this);
      }
    }
  }
}