﻿using System;
using UnityEngine;
using UnityEngine.Advertisements;

namespace Caviezel {
  namespace Monetization {
    public enum UnityAdsAudienceTarget {
      Family,
      Mixed,
      NonFamily
    }

    [Serializable]
    public class UnityAdsAudienceData {
      public UnityAdsAudienceTarget m_audienceTarget;
      public void Init(bool isAmazon) {
#if UNITY_ANDROID
        if (!isAmazon) {
          Debug.Log("**** unity ads init audience " + m_audienceTarget);
          if (m_audienceTarget == UnityAdsAudienceTarget.Family) {
            MetaData metaData = new MetaData("privacy");
            metaData.Set("mode", "app"); // This app is directed at children; no users will receive personalized ads.
            Advertisement.SetMetaData(metaData);
          } else if (m_audienceTarget == UnityAdsAudienceTarget.Mixed) {
            MetaData metaData = new MetaData("privacy");
            metaData.Set("mode", "mixed"); // This is a mixed audience game.
            Advertisement.SetMetaData(metaData);

            MetaData metaDataUser = new MetaData("user");
            metaDataUser.Set("nonbehavioral", "true"); // This user will NOT receive personalized ads.
            Advertisement.SetMetaData(metaDataUser);
          }
        }
#endif
      }

    }

    [Serializable]
    public struct UnityAdData {
      public string m_gameIdIOS;
      public string m_gameIdAndroid;
      public int m_showOrder;
      public bool m_shouldLoad;


      public string GameId {
        get {
#if UNITY_ANDROID
          return m_gameIdAndroid;
#elif UNITY_IPHONE
                    return m_gameIdIOS;
#endif
        }
      }
    }
  }
}