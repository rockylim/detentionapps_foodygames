﻿using UnityEngine.Advertisements;

namespace Caviezel {
  namespace Monetization {
    public class UnityAdInterstitial : IInterstitial, IUnityAdsInitializationListener, IUnityAdsLoadListener, IUnityAdsShowListener {
      bool m_ready;
      const string m_addUnit = "video";

      public UnityAdInterstitial(UnityAdData adData) {
        Order = adData.m_showOrder;
        if (!Advertisement.isInitialized) {
          UnityEngine.Debug.Log("unity ads : init interstitial ads");

          bool isTestMode = false;
#if UNITY_EDITOR
          isTestMode = true;
#endif          
          Advertisement.Initialize(adData.GameId, isTestMode);
        }
      }

      #region IInterstitial
      public int Order { get; private set; }

      public bool ShowInterstitial() {
        UnityEngine.Debug.Log("unity ads : try show unity interstitial ads");
#if UNITY_EDITOR
        m_ready = true;
#endif
        if (m_ready) {
          Advertisement.Show(m_addUnit);
          return true;
        }
        LoadAd();
        return false;
      }
      #endregion


      #region IUnityAdsInitializationListener
      public void OnInitializationComplete() {
        UnityEngine.Debug.Log("unity ads : init interstitial ads completed");
        LoadAd();
      }

      public void OnInitializationFailed(UnityAdsInitializationError error, string message) {
        UnityEngine.Debug.Log("unity ads : unity interstitial ads init failed " + message);
      }
      #endregion


      #region IUnityAdsLoadListener
      public void OnUnityAdsAdLoaded(string placementId) {
        if (m_addUnit.IsEqual(placementId)) {
          m_ready = true;
        }
      }

      public void OnUnityAdsFailedToLoad(string placementId, UnityAdsLoadError error, string message) {
        UnityEngine.Debug.Log("unity ads : unity interstitial ads failed to load " + message);
      }
      #endregion


      #region IUnityAdsShowListener
      public void OnUnityAdsShowClick(string placementId) { }
      public void OnUnityAdsShowComplete(string placementId, UnityAdsShowCompletionState showCompletionState) { }

      public void OnUnityAdsShowFailure(string placementId, UnityAdsShowError error, string message) {
        UnityEngine.Debug.Log($"unity ads : Error showing interstitial unity Ad Unit {placementId}: {error.ToString()} - {message}");
        LoadAd();
      }

      public void OnUnityAdsShowStart(string placementId) {
        LoadAd();
      }
      #endregion    

      void LoadAd() {
        UnityEngine.Debug.Log("unity ads : try to load unity interstitial ads");
        if (m_ready) {
          UnityEngine.Debug.Log("unity ads : unity interstitial ads is alreay loaded");
          return;
        }
        m_ready = false;
        Advertisement.Load(m_addUnit, this);
      }
    }
  }
}
