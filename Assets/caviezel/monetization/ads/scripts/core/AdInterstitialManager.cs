﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Caviezel {
  namespace Monetization {
    public class AdInterstitialManager : MonoBehaviour, IAdsManager {
      List<IInterstitial> m_interstitials = new List<IInterstitial>();
      CapacityData m_interval;

      public bool HasInitialized { get; private set; }

      public void Init(int maxInterval) {
        //set the flag to true
        HasInitialized = true;
        //set interval
        m_interval = new CapacityData(maxInterval);
      }

      public void Add(IInterstitial interstitial) {
        if (!m_interstitials.Contains(interstitial)) {
          m_interstitials.Add(interstitial);
        }
      }

      public void Show(Action OnShow = null) {
        if (!HasInitialized) {
          return;
        }
        //try show
        if (m_interstitials.Count > 0) {
          List<IInterstitial> candidates = new List<IInterstitial>(m_interstitials);
          while (true) {
            IInterstitial nearest = null;
            foreach (IInterstitial interstitial in candidates) {
              if (null == nearest) {
                nearest = interstitial;
              } else if (interstitial.Order < nearest.Order) {
                nearest = interstitial;
                //break; // removed so it will search the most lowert priority
              }
            }


            //if cant show, remove the nearest, still looping
            if (null != nearest && !nearest.ShowInterstitial()) {
              candidates.Remove(nearest);
            }
            //has shown, break from iteration
            else {
              if (OnShow != null) {
                OnShow();
              }
              break;
            }
          }
        }
      }

      public void AddInterval(Action OnShow = null) {
        if (!m_interval.Add()) {
          m_interval.Reset();
          Show(OnShow);
        }
      }

      #region IAdsManager
      public void UpdateAdManager(float dt) {
      }
      #endregion
    }
  }
}
