﻿namespace Caviezel {
    namespace Monetization {
        public delegate void OnSuccessShowingAd();

        public interface IInterstitial {
            int Order { get; }
            bool ShowInterstitial();
        }

        public interface IBanner {}

        public interface IReward {
            int Order { get; }
            bool ShowReward(IRewardAdListener listener);
        }

        public interface IRewardAdListener {
            void OnRewarded();
        }

        public interface IAdsManager {
            void UpdateAdManager(float dt);
        }
    }
}
