﻿using System.Collections.Generic;
using UnityEngine;
using Caviezel.Chrono;

namespace Caviezel {
    namespace Monetization {
        public class AdRewardManager : MonoBehaviour, IAdsManager, IRewardAdListener {
            public bool HasInitialized { get; private set; }

            HashSet<IReward> m_rewards = new HashSet<IReward>();
            IRewardAdListener m_rewardListener;
            Timer m_rewardedTimer;

            public void Add(IReward reward) {
                m_rewards.Add(reward);
            }

            public void Show(IRewardAdListener listener) {
                //try show
                if (m_rewards.Count > 0) {
                    m_rewardListener = listener;
                    HashSet<IReward> candidates = new HashSet<IReward>(m_rewards);
                    while (true) {
                        IReward nearest = null;
                        foreach (IReward rewarded in candidates) {
                            if (null == nearest) {
                                nearest = rewarded;
                            } else if (rewarded.Order < nearest.Order) {
                                nearest = rewarded;
                                //break;
                            }
                        }
                        //if cant show, remove the nearest, still looping
                        if (null != nearest && !nearest.ShowReward(this)) {
                            candidates.Remove(nearest);
                        }
                        //has shown, break from iteration
                        else {
                            break;
                        }
                    }
                }
            }

            #region IAdsManager
            public void UpdateAdManager(float dt) {
                if (null != m_rewardedTimer && !m_rewardedTimer.UpdateTimer(dt)) {
                    m_rewardedTimer = null;
                    if (null != m_rewardListener) {
                        m_rewardListener.OnRewarded();
                        m_rewardListener = null;
                    }
                }
            }
            #endregion

            #region IRewardAdListener
            public void OnRewarded() {
                m_rewardedTimer = new Timer(.1f);
            }
            #endregion
        }
    }
}
