﻿using UnityEngine;
using Caviezel.Core;

namespace Caviezel {
    namespace Monetization {
        public class AdSystem : MonoBehaviour, ISystem {
            IAdsManager[] m_adsManagers;

            #region ISystem implementation
            public void InitSystem() {
                m_adsManagers = GetComponentsInChildren<IAdsManager>(true);
            }

            public void StartSystem(CavEngine gameEngine) { }

            public void UpdateSystem(float dt) {
                for (int i = 0; i < m_adsManagers.Length; ++i) {
                    m_adsManagers[i].UpdateAdManager(dt);
                }
            }

            public void OnChangeScene(int index) { }
            #endregion

            public T GetManager<T>() where T : class, IAdsManager {
                foreach (IAdsManager manager in m_adsManagers) {
                    T t = manager as T;
                    if (null != t) {
                        return t;
                    }
                }
                return null;
            }
        }
    }
}
