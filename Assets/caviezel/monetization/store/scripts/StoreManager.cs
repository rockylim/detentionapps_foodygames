﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using Caviezel.Core;
using Caviezel.Generic;

namespace Caviezel {
    namespace Monetization {
        public class StoreManager : MonoBehaviour, ISystem {
            public GameObject m_viewObj;

            IStoreAdapter m_storeAdapter = null;
            Product[] m_prods;

            public Dictionary<string, StoreProductData> StoreProducts { get; private set; }

            public Product[] Prods { 
                get {return m_prods;}
                set {                    
                    m_prods = value;
                    if (null != View) {
                        View.RenderStoreView(m_prods);   
                    }
                }
            }

            public CMessenger Messenger { get; private set; }

            public IStoreView View { get; private set; }

            #region ISystem implementation
            public void InitSystem() {
                //init the store messenger
                Messenger = new CMessenger();
            }

            public void StartSystem(CavEngine engine) { 
                //init the store view
                if (null != m_viewObj) {
                    View = m_viewObj.GetComponent<IStoreView>();
                    View.InitStoreView(engine, this);   
                }
            }

            public void UpdateSystem(float dt) {}

            public void OnChangeScene(int index) {}
            #endregion

            public bool HasInitialized { get { return null != m_storeAdapter; }}

            public void InitStore(IEnumerable<StoreProductData> storeProducts, StoreType storeType) {
                //dont process if the store has been initialized previously
                if (HasInitialized) {
                    return;
                }
                //init the product datas
                StoreProducts = new Dictionary<string, StoreProductData>();
                foreach (StoreProductData prodData in storeProducts) {
                    StoreProducts.Add(prodData.m_id, prodData);
                }
                //create the store adapter
                m_storeAdapter = new StoreAdapterFactory().Create(storeType, this);
            }

            public void ShowStore(bool flag) {
                if (null != View) {
                    View.ShowStoreView(flag);
                }
            }

            public void PurchaseProduct(string productId) {
                Debug.Log("trying to find product id : " + productId);
                if (StoreProducts.ContainsKey(productId)) {
                    Debug.Log("id found , make purchase : " + productId);
                    m_storeAdapter.MakePurchase(productId);
                }
            }

            public void BroadcastPurchaseEvent<T>(T pe) {
                //broadcast the event
                Messenger.BroadcastMessage(pe);
                //hide the view if it's a success
                PostPurchaseSuccessEv successEv = pe as PostPurchaseSuccessEv;
                if (null != successEv && null != View) {
                    View.OnPurchased(successEv.PurchaseId);
                }
            }

            public void RestorePurchase() {
                print("restore purchase");
                m_storeAdapter.RestorePurchase();
            }
        }   
    }	
}

