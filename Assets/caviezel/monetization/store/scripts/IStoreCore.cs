﻿using System;
using System.Collections.Generic;
using Caviezel.Core;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Extension;

namespace Caviezel {
  namespace Monetization {
    [Serializable]
    public class StoreProductData {
      public string m_id;
      public ProductType m_type;
      public List<string> m_lockedScenes;
      public StoreProductData(string theId, ProductType theType, List<string> lockedScenes) {
        m_id = theId;
        m_type = theType;
        m_lockedScenes = lockedScenes;
      }

      public StoreProductData(string theId, List<string> lockedScenes) {
        m_id = theId;
        m_type = ProductType.Consumable;
        m_lockedScenes = lockedScenes;
      }
    }

    public interface IStoreInitializer {
      Dictionary<string, StoreProductData> GetStoreIDs();
    }

    public interface IStoreAdapter {
      bool HasStoreInitialized { get; }
      void MakePurchase(string productId);
      void RestorePurchase();
    }

    public interface IStoreView {
      void InitStoreView(CavEngine engine, StoreManager manager);
      void RenderStoreView(Product[] prods);
      void ShowStoreView(bool flag);
      void OnPurchased(string prodId);
    }

    public enum PurchasingStatus {
      PURCHASING
        , NOT_FOUND_ID
        , NOT_INITIALIZED
    }

    public enum RestorePurchaseStatus {
      START
        , FAILED
        , CONTINUE
        , SUCCESS
        , UNSUPPORTED_PLATFORM
    }

    public interface IStoreCallback { }

    public interface IStoreCallback<T> : IStoreCallback {
      void ReceiveEvent(T ev);
    }

    public class PrePurchasePassEv {
      public Product[] IAPProducts;
    }

    public class PrePurchaseFailEv {
      public InitializationFailureReason InitFailReason;
    }

    public class PostPurchaseSuccessEv {
      public string PurchaseId;
      public Product IAPProduct;
    }

    public class PostPurchaseFailureEv {
      public PurchaseFailureDescription FailureReason;
    }

    public class PurchasingEv {
      public PurchasingStatus Status;
    }

    public class RestoreEv {
      public RestorePurchaseStatus Status;
    }

    [Serializable]
    public enum StoreType {
      Unity
    }

    public class StoreAdapterFactory {
      delegate IStoreAdapter CreateStoreAdapter(StoreManager manager);

      IStoreAdapter CreateUnityAdapter(StoreManager manager) {
        return new UnityStoreAdapter(manager);
      }

      Dictionary<StoreType, CreateStoreAdapter> m_containers = new Dictionary<StoreType, CreateStoreAdapter>();

      public StoreAdapterFactory() {
        m_containers[StoreType.Unity] = CreateUnityAdapter;
      }

      public IStoreAdapter Create(StoreType type, StoreManager manager) {
        return m_containers[type](manager);
      }
    }
  }
}