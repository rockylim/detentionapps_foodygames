﻿using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Extension;

namespace Caviezel {
  namespace Monetization {
    public class UnityStoreAdapter : IDetailedStoreListener, IStoreAdapter {
      static IStoreController m_storeController;          // The Unity Purchasing system.
      static IExtensionProvider m_storeExtensionProvider; // The store-specific Purchasing subsystems.
      StoreManager m_storeManager;

      public bool HasStoreInitialized {
        get {
          // Only say we are initialized if both the Purchasing references are set.
          return m_storeController != null && m_storeExtensionProvider != null;
        }
      }

      public UnityStoreAdapter(StoreManager storeManager) {
        // If we haven't set up the Unity Purchasing reference
        if (m_storeController == null) {
          m_storeManager = storeManager;
          // Begin to configure our connection to Purchasing
          InitializePurchasing();
        }
      }

      public void InitializePurchasing(/*IStoreInitializer storeInitializer*/) {
        // If we have already connected to Purchasing ...
        if (HasStoreInitialized) {
          // ... we are done here.
          return;
        }
        // Create a builder, first passing in a suite of Unity provided stores.
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        foreach (var storeData in m_storeManager.StoreProducts) {
          builder.AddProduct(storeData.Key, storeData.Value.m_type);
        }

        UnityPurchasing.Initialize(this, builder);
      }

      public void MakePurchase(string productId) {
        // If Purchasing has been initialized ...
        if (HasStoreInitialized) {
          Product product = m_storeController.products.WithID(productId);
          // If the look up found a product for this device's store and that product is ready to be sold ... 
          if (product != null && product.availableToPurchase) {
            PurchasingEv pe = new PurchasingEv();
            pe.Status = PurchasingStatus.PURCHASING;
            m_storeManager.BroadcastPurchaseEvent(pe);
            m_storeController.InitiatePurchase(product);
          }
          // Otherwise ...
          else {
            PurchasingEv pe = new PurchasingEv();
            pe.Status = PurchasingStatus.NOT_FOUND_ID;
            m_storeManager.BroadcastPurchaseEvent(pe);
          }
        }
        // Otherwise ...
        else {
          PurchasingEv pe = new PurchasingEv();
          pe.Status = PurchasingStatus.NOT_INITIALIZED;
          m_storeManager.BroadcastPurchaseEvent(pe);
        }
      }

      // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
      // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
      public void RestorePurchase() {
        // If Purchasing has not yet been set up ...
        if (!HasStoreInitialized) {
          // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
          PurchasingEv pe = new PurchasingEv();
          pe.Status = PurchasingStatus.NOT_INITIALIZED;
          m_storeManager.BroadcastPurchaseEvent(pe);
          return;
        }

        // If we are running on an Apple device ... 
        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer) {
          // ... begin restoring purchases
          Debug.Log("RestorePurchases started ...");
          RestoreEv pe = new RestoreEv();
          pe.Status = RestorePurchaseStatus.START;
          m_storeManager.BroadcastPurchaseEvent(pe);

          // Fetch the Apple store-specific subsystem.
          var apple = m_storeExtensionProvider.GetExtension<IAppleExtensions>();
          // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
          // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
          apple.RestoreTransactions((result, error) => {
            // The first phase of restoration. If no more responses are received on ProcessPurchase then 
            // no purchases are available to be restored.
            RestoreEv ev = new RestoreEv();
            ev.Status = result ? RestorePurchaseStatus.SUCCESS : RestorePurchaseStatus.FAILED;
            m_storeManager.BroadcastPurchaseEvent(pe);
            Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
          });
        }
        // Otherwise ...
        else {
          RestoreEv pe = new RestoreEv();
          pe.Status = RestorePurchaseStatus.UNSUPPORTED_PLATFORM;
          m_storeManager.BroadcastPurchaseEvent(pe);
        }
      }

      //  
      // --- IStoreListener
      //
      public void OnInitialized(IStoreController controller, IExtensionProvider extensions) {
        // Overall Purchasing system, configured with products for this application.
        m_storeController = controller;
        // Store specific subsystem, for accessing device-specific store features.
        m_storeExtensionProvider = extensions;
        //store the products
        m_storeManager.Prods = m_storeController.products.all;
      }

      public void OnInitializeFailed(InitializationFailureReason error) {
        PrePurchaseFailEv pe = new PrePurchaseFailEv();
        pe.InitFailReason = error;
        m_storeManager.BroadcastPurchaseEvent(pe);
      }


      public void OnInitializeFailed(InitializationFailureReason reason, string error) {
        PrePurchaseFailEv pe = new PrePurchaseFailEv();
        pe.InitFailReason = reason;
        m_storeManager.BroadcastPurchaseEvent(pe);
      }

      public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) {
        PostPurchaseSuccessEv pe = new PostPurchaseSuccessEv();
        pe.IAPProduct = m_storeManager.StoreProducts.ContainsKey(args.purchasedProduct.definition.id) ? args.purchasedProduct : null;
        pe.PurchaseId = args.purchasedProduct.definition.id;
        //broadcast the purchase event
        m_storeManager.BroadcastPurchaseEvent(pe);
        // Return a flag indicating whether this product has completely been received, or if the application needs 
        // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
        // saving purchased products to the cloud, and when that save is delayed. 
        return PurchaseProcessingResult.Complete;
      }

      public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason) {
      }

      public void OnPurchaseFailed(Product product, PurchaseFailureDescription failureReason) {
        // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
        // this reason with the user to guide their troubleshooting actions.
        PostPurchaseFailureEv pe = new PostPurchaseFailureEv();
        pe.FailureReason = failureReason;
        m_storeManager.BroadcastPurchaseEvent(pe);
      }
    }
  }
}
