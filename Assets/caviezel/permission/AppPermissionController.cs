﻿using System;
using System.Collections.Generic;
using Caviezel.Core;
using UnityEngine;
#if UNITY_IOS
using Unity.Advertisement.IosSupport;
#endif

#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif

namespace Caviezel {
  namespace AppPermission {
    public class AppPermissionController : MonoBehaviour, ISystem {
      [Serializable]
      public class PermissionData {
        public bool m_active;
        public string m_permissionName;
        public bool m_showInInit;
        public int m_changeSceneTarget = -1;
        public CapacityData m_intervalSceneChanges;
      }
      [Header("IOS")]
      public List<PermissionData> m_iOSPermission;
      [Header("Android")]
      public List<PermissionData> m_androidPermission;
      bool m_isShowAds;
      List<PermissionData> m_activePermission;


      #region ISystem implementation
      public void InitSystem() {
        m_activePermission = new List<PermissionData>();
        m_activePermission = m_iOSPermission;
#if UNITY_IOS
        m_activePermission = m_iOSPermission;
#elif UNITY_ANDROID
        m_activePermission = m_androidPermission;
#endif
        m_activePermission.RemoveAll(permission => permission.m_active == false);
      }

      public void StartSystem(CavEngine gameEngine) {
        if (m_activePermission == null) {
          return;
        }
        for (int i = 0; i < m_activePermission.Count; i++) {
          if (m_activePermission[i].m_showInInit) {
            ShowPermission(m_activePermission[i].m_permissionName);
          }

          if (m_activePermission[i].m_changeSceneTarget > -1) {
            m_activePermission[i].m_intervalSceneChanges = new CapacityData(m_activePermission[i].m_changeSceneTarget);
          }
        }
      }

      public void UpdateSystem(float dt) {
      }

      public void OnChangeScene(int index) {
      }
      #endregion     

      public void ShowPermission(string permissionType) {
        if (m_isShowAds) {
          if (permissionType == "ATT") {
#if UNITY_EDITOR
            Debug.Log("*********************** EDITOR DEBUG : show ATT iOS Prompt *********************** ");
#endif
#if UNITY_IOS
            if (ATTrackingStatusBinding.GetAuthorizationTrackingStatus() ==
                      ATTrackingStatusBinding.AuthorizationTrackingStatus.NOT_DETERMINED) {
              ATTrackingStatusBinding.RequestAuthorizationTracking();
            }
#endif
          }

          if (permissionType == "Storage") {
#if UNITY_EDITOR
            Debug.Log("*********************** EDITOR DEBUG : show storage android prompt *********************** ");
#endif
#if UNITY_ANDROID
            if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite)) {
              Permission.RequestUserPermission(Permission.ExternalStorageWrite);
            }
#endif
          }
        }
      }

      public void AddChangeSceneCount() {
        if (m_activePermission == null) {
          return;
        }
        for (int i = 0; i < m_activePermission.Count; i++) {
          if (m_activePermission[i].m_active) {
            if ((m_activePermission[i].m_intervalSceneChanges != null) && (m_activePermission[i].m_intervalSceneChanges.Max > -1)) {
              if (!m_activePermission[i].m_intervalSceneChanges.Add()) {
                m_activePermission[i].m_active = false;
                ShowPermission(m_activePermission[i].m_permissionName);
              }
            }
          }
        }
      }

      public void SetAdsStatus(bool isShowAds) {
        m_isShowAds = isShowAds;
      }
    }
  }
}