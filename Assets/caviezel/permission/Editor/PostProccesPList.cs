﻿using UnityEditor;
using UnityEditor.Callbacks;
#if UNITY_IOS
using UnityEditor.iOS.Xcode;
using System.IO;
#endif


namespace Caviezel {
  namespace AppPermission {
    public class PostProcessATT {

      //setting here!
      const bool m_addUserTrackingUsageDescription = true;
      const bool m_addSDKNetworkID = true;

      const string TrackingDescription =
          "This identifier will be used to deliver personalized ads to you.";

      [PostProcessBuild(0)]
      public static void OnPostprocessBuild(BuildTarget buildTarget, string pathToXcode) {
#if UNITY_IOS
        if (buildTarget == BuildTarget.iOS) {
          AddPListValues(pathToXcode);
        }
#endif
      }

      static void AddPListValues(string pathToXcode) {
#if UNITY_IOS
        // Get Plist from Xcode project 
        string plistPath = pathToXcode + "/Info.plist";

        // Read in Plist 
        PlistDocument plistObj = new PlistDocument();
        plistObj.ReadFromString(File.ReadAllText(plistPath));

        // set values from the root obj
        PlistElementDict plistRoot = plistObj.root;
        if (m_addUserTrackingUsageDescription) {
          plistRoot.SetString("NSUserTrackingUsageDescription", TrackingDescription);
        }

        if (m_addSDKNetworkID) {
          PlistElementArray array = plistRoot.CreateArray("SKAdNetworkItems");
          array.AddDict().SetString("SKAdNetworkIdentifier", "bvpn9ufa9b.skadnetwork"); // unity ads
          array.AddDict().SetString("SKAdNetworkIdentifier", "4dzt52r2t5.skadnetwork"); // unity ads
          array.AddDict().SetString("SKAdNetworkIdentifier", "cstr6suwn9.skadnetwork"); // google admob
          array.AddDict().SetString("SKAdNetworkIdentifier", "v79kvwwj4g.skadnetwork"); // kidoz
          array.AddDict().SetString("SKAdNetworkIdentifier", "ludvb6z3bs.skadnetwork"); // applovin          
        }

        // save
        File.WriteAllText(plistPath, plistObj.WriteToString());
#endif
      }

    }
  }
}