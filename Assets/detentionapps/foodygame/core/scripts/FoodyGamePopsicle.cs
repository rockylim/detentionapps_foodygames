﻿using System.Collections.Generic;
using Caviezel;
using Caviezel.Core;
using Caviezel.Pretend;
using UnityEngine;
using UnityEngine.UI;

namespace Detentionapps {
  namespace IceCream {
    public class FoodyGamePopsicle : PtdComponentActive, IUpdateableComponent {

      public Sprite m_pixelSprite;
      public RectTransform m_splashParticle;

      private List<Image> m_colorFillImages;
      private float m_lastFillPosition;
      private int m_lastColor = -1;
      private int m_selectedColor = 0;
      private int m_lastIndex = -1;
      private int m_panelPressed = 0;
      private bool m_isLock = false;
      bool m_isUseSplash = false;

      const float m_firstFillImagePosition = -282f;
      const float m_fillWidth = 564f;
      const float m_fillIncrement = 70f;
      const float m_mixOffset = 0f;
      const float m_fillYTarget = 250f;
      const float m_fillYLock = 210f;


      #region PtdComponentActive
      public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
        base.InitComponent(sceneStarter, gameController);

        // init var
        m_colorFillImages = new List<Image>();
        m_lastFillPosition = m_firstFillImagePosition;
        if (m_splashParticle != null) {
          m_isUseSplash = true;
          m_splashParticle.SetY(m_lastFillPosition);
        }
      }

      public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
        base.OnReceiveEvent(ev, scope);
        if (IsActive) {
          //print(ev.m_senderId);
          //print(ev.m_eventId);
          //print(scope);
          if (ev.m_senderId == "icepanel") {
            if (ev.m_eventId == "ev_panel_press") {
              PanelPressed();
            } else if (ev.m_eventId == "ev_panel_release") {
              PanelUnpressed();
            }
          } else if ((ev.m_senderId.Contains("flavor")) && (ev.m_eventId.Contains("flavor")) && (ev.m_senderId != "buttonflavor")) {
            int.TryParse(ev.m_eventId.Substring(6, ev.m_eventId.Length - 6), out m_selectedColor);
          }
          return true;
        }
        //return false if the component is not active, meaning it doesnt receive any events
        return false;
      }
      #endregion

      #region IUpdateableComponent
      public void UpdateComponent(float dt) {

        if (m_panelPressed == 1) {
          Image activeImage = m_colorFillImages[m_lastIndex];
          float yIncrement = m_fillIncrement * dt;
          float currentHeight = activeImage.rectTransform.Height();
          activeImage.rectTransform.SetSize(new Vector2(activeImage.rectTransform.Width(), currentHeight + yIncrement));
          m_lastFillPosition = activeImage.rectTransform.Y() + activeImage.rectTransform.Height();
          if (m_isUseSplash) {
            m_splashParticle.SetY(m_lastFillPosition);
          }
          // move to refresh mask
          activeImage.rectTransform.AddPosX(.000001f);

          CheckLock();
          CheckDone();
        }
      }
      #endregion

      private void PanelPressed() {
        m_panelPressed = 1;
        if (m_selectedColor != m_lastColor) {
          m_lastColor = m_selectedColor;
          CreateNewColorFill();
        }
      }

      private void PanelUnpressed() {
        if (m_isLock == true) {
          return;
        }
        Image activeImage = m_colorFillImages[m_lastIndex];
        m_panelPressed = 0;
      }

      private void CreateNewColorFill() {
        m_lastIndex++;
        m_lastFillPosition = m_lastFillPosition + m_mixOffset;//mix effct;

        GameObject newFill = new GameObject("fill");
        Image newImage = newFill.AddComponent<Image>();
        newImage.sprite = m_pixelSprite;
        newImage.rectTransform.SetParent(gameObject.transform);
        newImage.GetComponent<RectTransform>().pivot = new Vector2(.5f, .0f);
        newImage.rectTransform.SetPos(new Vector2(.0f, m_lastFillPosition));
        newImage.rectTransform.SetSize(new Vector2(m_fillWidth, .0001f));
        newImage.rectTransform.SetScale(new Vector3(1f, 1f, 1f));
        SetColor(newImage);
        m_colorFillImages.Add(newImage);
      }

      private void SetColor(Image image) {
        switch (m_selectedColor) {
          case 1:
            image.color = new Color32(255, 236, 16, 250);
            break;
          case 2:
            image.color = new Color32(4, 103, 188, 250);
            break;
          case 3:
            image.color = new Color32(131, 219, 254, 250);
            break;
          case 4:
            image.color = new Color32(125, 0, 17, 250);
            break;
          case 5:
            image.color = new Color32(84, 38, 11, 250);
            break;
          case 6:
            image.color = new Color32(109, 51, 5, 250);
            break;
          case 7:
            image.color = new Color32(43, 23, 9, 250);
            break;
          case 8:
            image.color = new Color32(255, 225, 179, 250);
            break;
          case 9:
            image.color = new Color32(146, 237, 85, 250);
            break;
          case 10:
            image.color = new Color32(98, 243, 208, 250);
            break;
          case 11:
            image.color = new Color32(255, 136, 20, 250);
            break;
          case 12:
            image.color = new Color32(238, 207, 111, 250);
            break;
          case 13:
            image.color = new Color32(255, 96, 172, 250);
            break;
          case 14:
            image.color = new Color32(220, 28, 33, 250);
            break;
          case 15:
            image.color = new Color32(249, 222, 140, 250);
            break;
          default:
            image.color = new Color32(255, 255, 255, 250);
            break;
        }
        if (m_isUseSplash) {
          List<PtdParticle> particles = m_splashParticle.GetComponent<PtdParticleController>().GetParticles();
          for (int i = 0; i < particles.Count; i++) {
            particles[i].GetComponent<Image>().color = image.color;
          }
        }
      }

      private void CheckLock() {
        if (m_lastFillPosition >= m_fillYLock) {
          BroadcastEvent(new BaseSenderEv("selectedshape", "ev_fill_lock"));
          m_isLock = true;
        }
      }

      private void CheckDone() {
        if (m_lastFillPosition >= m_fillYTarget) {
          BroadcastEvent(new BaseSenderEv("selectedshape", "ev_fill_done"));
          m_panelPressed = 0;
        }
      }
    }
  }
}