﻿using Caviezel.Core;
using Caviezel.Pretend;
using Caviezel.UI;
using UnityEngine;

namespace Detentionapps {
  namespace FoodyGames {
    public class FoodyGameStoreButton : PtdComponentBase {
      public string m_IAPId;
      FoodyGameSystem m_system;
      CGestureHandler m_tapHandler;

      public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
        base.InitComponent(sceneStarter, gameController);
        //cache system
        m_system = sceneStarter.Engine.GetSystem<FoodyGameSystem>();
        Debug.Assert(null != m_system);
        //init tap handler
        m_tapHandler = new CGestureHandler(gameObject);
        m_tapHandler.SetTappable();
        m_tapHandler.OnTapListeners += OnTap;
      }

      void OnTap(Vector2 pos) {
        m_system.BuyIAP(m_IAPId);
      }
    }
  }
}
