﻿using System;
using Caviezel.Core;
using Caviezel.Pretend;
using UnityEngine;


namespace Detentionapps {
  namespace FoodyGames {

    public class FoodyGamePurchaseIAP : PtdComponentActive {
      [Serializable]
      public struct PurchaseIAPData {
        public string m_IAPId;
        public BaseReceiverEv[] m_onPurchase;
      }
      FoodyGameSystem m_system;
      public PurchaseIAPData[] m_purchaseEvs;


      public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
        base.InitComponent(sceneStarter, gameController);

        // get main system
        m_system = sceneStarter.Engine.GetSystem<FoodyGameSystem>();
        Debug.Assert(null != m_system);
      }

      public override void OnStartGame() {
        base.OnStartGame();
      }

      public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
        base.OnReceiveEvent(ev, scope);
        if (IsActive) {
          for (int i = 0; i < m_purchaseEvs.Length; ++i) {
            for (int j = 0; j < m_purchaseEvs[i].m_onPurchase.Length; j++) {
              if (m_purchaseEvs[i].m_onPurchase[j].Matches(ev, scope)) {
                m_system.BuyIAP(m_purchaseEvs[i].m_IAPId);
                return true;
              }
            }

          }
        }
        return false;
      }
    }
  }
}