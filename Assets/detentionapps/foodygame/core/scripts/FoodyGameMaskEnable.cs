﻿using System;
using Caviezel.Core;
using Caviezel.Pretend;
using UnityEngine.UI;

namespace Detentionapps {
  namespace IceCream {
    public class FoodyGameMaskEnable : PtdComponentActive {
      public MaskEnableEvent[] m_maskActivatorEvs;

      [Serializable]
      public class MaskEnableEvent : BaseReceiverEv {
        public RectMask2D m_mask;
        public bool m_flag;
      }

      #region PtdComponentActive
      public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
        base.InitComponent(sceneStarter, gameController);
      }

      public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
        base.OnReceiveEvent(ev, scope);
        if (IsActive) {
          for (int i = 0; i < m_maskActivatorEvs.Length; ++i) {
            if (m_maskActivatorEvs[i].Matches(ev, scope)) {
              m_maskActivatorEvs[i].m_mask.enabled = m_maskActivatorEvs[i].m_flag;
            }
          }
        }
        return false;
      }
      #endregion
    }
  }
}