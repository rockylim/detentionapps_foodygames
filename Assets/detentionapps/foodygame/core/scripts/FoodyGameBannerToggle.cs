﻿using System;
using Caviezel.Core;
using Caviezel.Pretend;
using UnityEngine;

namespace Detentionapps {
  namespace FoodyGames {
    public class FoodyGameBannerToggle : PtdComponentActive {

      [Serializable]
      public class BannerToggleEv : BaseReceiverEv {
        public bool m_flag;
      }

      public BannerToggleEv[] m_toggleEvs;
      public bool m_shouldToggleOnAwake;
      [ConditionalHide("m_shouldToggleOnAwake", true)]
      public bool m_flag;

      FoodyGameSystem m_system;

      public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
        base.InitComponent(sceneStarter, gameController);
        m_system = sceneStarter.Engine.GetSystem<FoodyGameSystem>();
        if (m_shouldToggleOnAwake) {
          m_system.ShowBanner(m_flag);
        }
      }

      public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
        base.OnReceiveEvent(ev, scope);
        if (IsActive) {
          for (int i = 0; i < m_toggleEvs.Length; ++i) {
            if (m_toggleEvs[i].Matches(ev, scope)) {
              m_system.ShowBanner(m_toggleEvs[i].m_flag);
              return true;
            }
          }
        }
        return false;
      }
    }
  }
}
