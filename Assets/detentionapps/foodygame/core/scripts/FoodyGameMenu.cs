﻿using System;
using Brainfull.CrossPromo;
using Caviezel.Audio;
using Caviezel.Core;
using Caviezel.Monetization;
using Caviezel.Pretend;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Detentionapps {
  namespace FoodyGames {
    public class FoodyGameMenu : MonoBehaviour, ISceneController {
      [Serializable]
      public struct MenuStartSfx {
        public string m_sfxName;
        public float m_delay;
      }

      public GameObject m_soundOffBtn;
      public GameObject m_BGMOffBtn;
      public GameObject[] m_storeBtns;
      public MenuStartSfx[] m_onStartSfx;

      public string m_defaultPlayScene;
      public PtdParticleController[] m_particleController;

      FoodyGameSystem m_system;
      StoreManager m_storeManager;
      SfxManager m_sfxManager;
      BCCrossPromo m_crossPromo;
      bool m_crossPromoChecked;
      #region ISceneController
      public void InitSceneController(ISceneStarter sceneStarter) {
        m_crossPromo = sceneStarter.Engine.GetSystem<BCCrossPromo>();
        Debug.Assert(null != m_crossPromo);

        m_system = sceneStarter.Engine.GetSystem<FoodyGameSystem>();
        for (int i = 0; i < m_particleController.Length; i++) {
          m_particleController[i].InitParticle();
          m_particleController[i].OnParticleStart();
          m_particleController[i].IsActive = true;
        }
      }

      public void StartSceneController(ISceneStarter sceneStarter) {
        m_system.ShowBanner(true);
        //toggle the store buttons accordingly

        m_sfxManager = sceneStarter.Engine.GetSystem<AudioSystem>().GetManager<SfxManager>();
        Debug.Assert(null != m_sfxManager);

        m_storeManager = sceneStarter.Engine.GetSystem<StoreManager>();

        for (int i = 0; i < m_storeBtns.Length; ++i) {
          m_storeBtns[i].SetActive(m_system.m_iapOn);
        }
        //render the sound btn accordingly
        RenderSoundBtn();

        //render the bgm btn accordingly
        RenderBGMBtn();

        PlayOnStartSfx();
      }

      public void UpdateSceneController(float dt) {
        if ((m_crossPromo.IsDownloaded()) && (!m_crossPromoChecked)) {
          m_crossPromoChecked = true;
          m_crossPromo.TryShow(() => m_crossPromo.SetVisibleIconGroup(true));
        }
        for (int i = 0; i < m_particleController.Length; i++) {
          m_particleController[i].UpdateParticle(dt);
        }
      }
      #endregion

      #region Btn Events
      public void ToGame() {
        m_crossPromo.TryShow(() => m_crossPromo.SetVisibleIconGroup(false));
        StopOnStartSfx();
        m_sfxManager.PlaySound("pop");
        SceneManager.LoadScene(m_defaultPlayScene);
      }

      public void OnMoreGames1() {
#if UNITY_IOS || UNITY_STANDALONE_OSX
        Application.OpenURL(m_system.MoreIOS1);
#endif
#if UNITY_ANDROID
        if (!Constants.IsAmazon) {
          Application.OpenURL(m_system.MoreAndroid1);
        } else {
          Application.OpenURL(m_system.MoreAmazon1);
        }

#endif
      }

      public void ToggleSound() {
        m_system.IsSoundOn = !m_system.IsSoundOn;
        RenderSoundBtn();
      }

      public void ToggleBGM() {
        m_system.IsBGMOn = !m_system.IsBGMOn;
        RenderBGMBtn();
      }


      public void RestorePurchase() {
        m_system.RestorePurchase();
      }

      public void ShowStore(bool flag) {
        m_sfxManager.PlaySound("pop");
        m_storeManager.ShowStore(flag);
      }
      #endregion

      void RenderSoundBtn() {
        if (m_soundOffBtn != null) {
          m_soundOffBtn.SetActive(!m_system.IsSoundOn);
        }
      }

      void RenderBGMBtn() {
        if (m_BGMOffBtn != null) {
          m_BGMOffBtn.SetActive(!m_system.IsBGMOn);
        }
      }

      void PlayOnStartSfx() {
        if (m_onStartSfx != null) {
          for (int i = 0; i < m_onStartSfx.Length; i++) {
            m_sfxManager.PlaySound(m_onStartSfx[i].m_sfxName, 1, m_onStartSfx[i].m_delay);
          }
        }
      }

      void StopOnStartSfx() {
        if (m_onStartSfx != null) {
          for (int i = 0; i < m_onStartSfx.Length; i++) {
            m_sfxManager.StopAudioByName(m_onStartSfx[i].m_sfxName);
          }
        }
      }
    }
  }
}
