﻿using System.Collections.Generic;
using Caviezel;
using Caviezel.Pretend;
using Caviezel.UI;
using UnityEngine;

namespace Detentionapps {
  namespace FoodyGames {
    public class FoodyGameScoop : PtdDraggable {
      public RectTransform m_myArea;
      public RectTransform m_scoopArea;
      public bool m_useMultipleScoopArea;
      [ConditionalHide("m_useMultipleScoopArea", true)]
      public List<RectTransform> m_scoopAreas;
      public RectTransform m_pourArea;
      public BaseSenderEv m_scoopEv;
      public BaseSenderEv m_scoopTargetNameEv;
      public BaseSenderEv m_pourEv;
      public BaseSenderEv m_endDragEv;
      public BaseSenderEv m_doneEv;
      public int m_maxCount;

      bool m_isScooping;
      int m_counter;

      public override void OnStartGame() {
        base.OnStartGame();
        if (!m_useMultipleScoopArea) {
          m_scoopAreas = new List<RectTransform>();
          m_scoopAreas.Add(m_scoopArea);
        }
      }

      public override void OnSwiping(SwipeEventData ev) {
        base.OnSwiping(ev);
        if (m_counter < m_maxCount) {
          if (!m_isScooping) {
            for (int i = 0; i < m_scoopAreas.Count; i++) {
              RectTransform scoopArea = m_scoopAreas[i];
              if (m_myArea.RectAgainstRoot().Intersects(scoopArea.RectAgainstRoot())) {
                m_isScooping = true;
                BaseSenderEv scoopEv = new BaseSenderEv(m_scoopEv);
                scoopEv.m_eventId += (m_counter + 1).ToString();
                BroadcastEvent(scoopEv);

                BaseSenderEv scoopTargetEv = new BaseSenderEv(m_scoopTargetNameEv);
                scoopTargetEv.m_eventId += scoopArea.gameObject.name;
                BroadcastEvent(scoopTargetEv);
              }
            }
          } else {
            if (m_myArea.RectAgainstRoot().Intersects(m_pourArea.RectAgainstRoot())) {
              m_isScooping = false;
              BaseSenderEv pourEv = new BaseSenderEv(m_pourEv);
              pourEv.m_eventId += (m_counter + 1).ToString();
              BroadcastEvent(pourEv);
              ++m_counter;
              if (m_counter >= m_maxCount) {
                BroadcastEvent(m_doneEv);
              }
            }
          }
        }
      }

      public override void OnEndSwipe(SwipeEventData ev) {
        base.OnEndSwipe(ev);
        BroadcastEvent(m_endDragEv);
      }
    }
  }
}
