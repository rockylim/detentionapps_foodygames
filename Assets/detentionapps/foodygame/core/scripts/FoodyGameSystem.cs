﻿using System;
using System.Collections.Generic;
using Caviezel.AppPermission;
using Caviezel.Audio;
using Caviezel.Core;
using Caviezel.Monetization;
using Caviezel.Pretend;
using Caviezel.Serialization;
using GoogleMobileAds.Api;
using GoogleMobileAds.Common;
using Unity.Services.Core;
using Unity.Services.Core.Environments;
//using KidozSDK;
using UnityEngine;
using UnityEngine.Advertisements;

namespace Detentionapps {
  namespace FoodyGames {
    public class FoodyGameSystem : PtdSystem, Caviezel.Generic.IObserver<PostPurchaseSuccessEv> {
      #region More Games URL
      [Header("More Games Link")]
      public string MoreAndroid1 = "https://play.google.com/store/apps/developer?id=Detention+Apps";
      public string MoreIOS1 = "https://apps.apple.com/us/developer/detention-apps/id999167300";
      public string MoreAmazon1 = "https://www.amazon.com/s?i=mobile-apps&rh=p_4%3ADetention+Apps&search-type=ss";
      #endregion

      #region IAP
      [Header("IAP")]
      public bool m_iapOn;
      [ConditionalHide("m_iapOn", true)]
      public List<StoreProductData> m_IAPProduct;
      #endregion

      #region ADS
      [Header("ADS")]
      //[ConditionalHide("m_iapOn", true)]
      // public KidozData m_kidozData;

      [ConditionalHide("m_iapOn", true)]
      public UnityAdsAudienceData m_unityAdsAudience;
      [Header("REWARDED")]
      [ConditionalHide("m_iapOn", true)]
      public UnityAdData m_rewardUnity;
      [Header("INTERSTITIAL")]
      [ConditionalHide("m_iapOn", true)]
      public int m_interstitialAdsFrequency = 3;
      [ConditionalHide("m_iapOn", true)]
      public InterstitialAdmobData m_interstitialAdmob;
      [ConditionalHide("m_iapOn", true)]
      public InterstitialAppLovinData m_interstitialAppLovin;
      //[ConditionalHide("m_iapOn", true)]
      //public InterstitialKidozData m_interstitialKidoz;
      [ConditionalHide("m_iapOn", true)]
      public UnityAdData m_interstitialUnity;
      [Header("BANNER")]
      public BannerAdmobData m_bannerAdmob;
      public bool m_hideBanner;
      public bool m_hideBannerInMenu;
      public bool m_hideInterstitialOnMenu;
      public bool m_skipPermissionCountOnMenu;
      #endregion

      #region BGM
      public bool m_isUsingOneBGM;
      #endregion

      AdSystem m_adsSystem;
      AdInterstitialManager m_interstitialManager;
      AppPermissionController m_permissionController;
      SfxManager m_sfxManager;
      GamePersisters m_persisters;
      PtdPersister m_ptdPersister;
      StoreManager m_store;
      BannerView m_banner;
      bool m_bannerLoaded;
      int m_currentScene;

      bool m_initGameServicesDone;
      bool m_initIAPDone;

      const string m_bgmMenu = "bg_kids_menu";
      const string m_bgmGame = "bg_kids_gameplay";
      string m_currentBGM = "";

      string m_purchasedIAP = "";

      // purchased counter is a delay to let other script know, that purchasing is happening and done
      int m_purchasedCounter = 0;
      public string MoreGameLink {
        get {
#if UNITY_IOS || UNITY_STANDALONE_OSX
          return MoreIOS1;
#elif UNITY_ANDROID
          return MoreAndroid1;
#endif
        }
      }

      #region Public Methods
      public void ShowBanner(bool flag) {
        print(flag ? "show banner" : "hide banner");
#if (UNITY_EDITOR && !UNITY_IOS) || !UNITY_EDITOR                      
        if (!m_bannerLoaded) {
          return;
        }

        if ((m_currentScene == 0) && (m_hideBannerInMenu)) {
          return;
        }

        if (flag) {
          if (ShouldShowAds()) {
            m_banner.Show();
          }
        } else {
          m_banner.Hide();
        }
#endif        
      }

      public bool IsSoundOn {
        get { return m_sfxManager.Volume > 0f; }
        set { m_sfxManager.Volume = value ? 1f : 0f; }
      }

      public bool IsBGMOn {
        get { return m_sfxManager.BGMVolume > 0f; }
        set { m_sfxManager.BGMVolume = value ? 1f : 0f; }
      }

      public bool ShouldShowAds() {
        //dont show ads if iap is not on
        if (!m_iapOn) {
          return false;
        }
        //otherwise dont show ads if any has been purchased
        return !m_ptdPersister.HasAnyProduct();
      }

      public void RestorePurchase() {
        m_store.RestorePurchase();
      }

      public string GetCurrentBGM() {
        return m_currentBGM;
      }

      public void IncrementAdCounter() {
        if (ShouldShowAds()) {
          m_interstitialManager.AddInterval();
        }
      }

      public void BuyIAP(string id) {
        m_store.PurchaseProduct(id);
      }

      public string GetPurchashedIAP() {
        return m_purchasedIAP;
      }
      #endregion

      #region Store Observer
      public void ReceiveMessage(PostPurchaseSuccessEv ev) {
        StoreProductData storeData = m_IAPProduct.Find(x => x.m_id == ev.PurchaseId);
        if (storeData != null) {
          m_ptdPersister.SetProduct(storeData.m_id);
          m_purchasedIAP = storeData.m_id;
          m_purchasedCounter = 0;
          ShowBanner(false);
          m_bannerLoaded = false;
        }
        //save
        m_persisters.SaveAll();
        m_permissionController.SetAdsStatus(ShouldShowAds());
      }
      #endregion

      public override void StartSystem(CavEngine gameEngine) {
        base.StartSystem(gameEngine);
        //get the store manager
        m_store = gameEngine.GetSystem<StoreManager>();
        Debug.Assert(null != m_store);
        m_store.Messenger.AddObserver(this);
        //cache the ads system
        m_adsSystem = gameEngine.GetSystem<AdSystem>();
        //get the persister
        m_persisters = gameEngine.GetSystem<GamePersisters>();
        Debug.Assert(null != m_persisters);
        m_ptdPersister = m_persisters.GetPersister<PtdPersister>();
        Debug.Assert(null != m_ptdPersister);
        //get sfx manager
        m_sfxManager = gameEngine.GetSystem<AudioSystem>().GetManager<SfxManager>();
        Debug.Assert(null != m_sfxManager);
        //set sfx vol
        m_sfxManager.Volume = m_ptdPersister.SoundVolume;
        m_sfxManager.BGMVolume = m_ptdPersister.BGMVolume;
        // set menu and game music name
        m_sfxManager.SetBGMName(m_bgmMenu, m_bgmGame);

        //play bg music
        if (!m_sfxManager.IsSoundPlaying(m_bgmGame)) {
          m_sfxManager.PlaySound(m_bgmGame, -1);
        }

        m_permissionController = gameEngine.GetSystem<AppPermissionController>();
        m_permissionController.SetAdsStatus(ShouldShowAds());
        Debug.Assert(null != m_permissionController);

        InitGamingServices();

        //check if iap is on
        if (m_iapOn) {
          //init the ads
          if (ShouldShowAds()) {
            //get ad system
            AdSystem adSystem = gameEngine.GetSystem<AdSystem>();

            // set unity audience
            m_unityAdsAudience.Init(Constants.IsAmazon);

            //init reward manager
            AdRewardManager rewardManager = adSystem.GetManager<AdRewardManager>();
            Debug.Assert(null != rewardManager);
            //load unity rewarded
            if ((m_rewardUnity.m_shouldLoad) && (m_rewardUnity.GameId != "")) {
              rewardManager.Add(new UnityAdReward(m_rewardUnity));
            }
            //get interstitial manager
            m_interstitialManager = m_adsSystem.GetManager<AdInterstitialManager>();
            Debug.Assert(null != m_interstitialManager);
            if (!m_interstitialManager.HasInitialized) {
              m_interstitialManager.Init(m_interstitialAdsFrequency);
              //init admob
              if (m_interstitialAdmob.m_shouldLoad) {
#if (UNITY_EDITOR && !UNITY_IOS) || !UNITY_EDITOR              
                m_interstitialManager.Add(new AdMobInterstitial(m_interstitialAdmob));
#endif                
              }
              //init applovin
              if (m_interstitialAppLovin.m_shouldLoad) {
                m_interstitialManager.Add(new AppLovinInterstitial(m_interstitialAppLovin));
              }
              //init unity
              if (m_interstitialUnity.m_shouldLoad) {
                m_interstitialManager.Add(new UnityAdInterstitial(m_interstitialUnity));
              }

              //init kidoz
              /*if (m_kidozData.m_shouldLoad) {
                Kidoz.Create();
#if !UNITY_EDITOR
                Kidoz.init(m_kidozData.m_publisherId, m_kidozData.m_securityToken);
                Kidoz.initSuccess += (obj) => {
                  print("kidoz init success, init interstitial");
                  m_interstitialManager.Add(new KidozInterstitial(m_interstitialKidoz.m_showOrder));
                };
#endif
              }*/
            }

            if (!m_hideBanner) {
#if (UNITY_EDITOR && !UNITY_IOS) || !UNITY_EDITOR              
              //init app id
              MobileAds.Initialize((InitializationStatus initstatus) => {
                // Callbacks from GoogleMobileAds are not guaranteed to be called on
                // main thread.
                // In this example we use MobileAdsEventExecutor to schedule these calls on
                // the next Update() loop.
                MobileAdsEventExecutor.ExecuteInUpdate(() => {

                  /*
                         List<string> deviceIds = new List<string>();
                         deviceIds.Add("6802DCD837792ABF057BCF4764051F5F");

                         RequestConfiguration requestConfiguration = new RequestConfiguration.Builder()
                         .SetTestDeviceIds(deviceIds)
                         .SetTagForChildDirectedTreatment(TagForChildDirectedTreatment.True)
                         .build();
                         MobileAds.SetRequestConfiguration(requestConfiguration);
                         */

                  //load banner
                  AdRequest request = new AdRequest.Builder().Build();
                  m_banner = new BannerView(m_bannerAdmob.UnitId.Trim(), AdSize.SmartBanner, AdPosition.Bottom);
                  m_banner.OnAdLoaded += (sender, args) => {
                    m_bannerLoaded = true;
                    if (m_currentScene == 0) {
                      // check if should show on mainmenu after load?
                      ShowBanner(!m_hideBannerInMenu);
                    }
                  };
                  m_banner.OnAdFailedToLoad += (sender, e) => {
                    print(e.LoadAdError.GetMessage());
                  };
                  m_banner.LoadAd(request);
                });
              });
#endif
            }
          }
        }
      }

      public override bool CanOpenScene(string id) {
        if (!m_iapOn) {
          return true;
        }
        //try find the store data that has this scene id
        for (int i = 0; i < m_IAPProduct.Count; ++i) {
          if (m_IAPProduct[i].m_lockedScenes.FindIndex(y => y.Equals(id, StringComparison.OrdinalIgnoreCase)) != -1) {
            //check whether the product has been purchased, if not, then prompt the iap
            bool productPurchased = m_ptdPersister.ProductExists(m_IAPProduct[i].m_id);
            if (!productPurchased) {
              //m_storeView.Show(m_lockScenesProducts[i]);
              BuyIAP(m_IAPProduct[i].m_id);
              return false;
            }
          }
        }
        //otherwise return true
        return true;
      }

      public override void UpdateSystem(float dt) {
        if (m_initGameServicesDone && !m_initIAPDone) {
          InitStore(m_IAPProduct);
          m_initIAPDone = true;
        }

        if (m_purchasedIAP != "") {
          m_purchasedCounter++;
          if (m_purchasedCounter == 2) {
            m_purchasedIAP = "";
            m_purchasedCounter = 0;
          }
        }
      }

      public override void OnChangeScene(int index) {
        base.OnChangeScene(index);
        m_currentScene = index;

        if (m_isUsingOneBGM) { // if only using 1 bgm play once when menu appear
          if (!m_sfxManager.IsSoundPlaying(m_bgmMenu)) {
            m_sfxManager.PlaySound(m_bgmMenu, -1);
            m_currentBGM = m_bgmMenu;
          }
        } else {
          if (index == 0) {
            m_sfxManager.StopAudioByName(m_bgmGame);
            m_sfxManager.PlaySound(m_bgmMenu, -1);
            m_currentBGM = m_bgmMenu;
          } else {
            m_sfxManager.StopAudioByName(m_bgmMenu);
            if (!m_sfxManager.IsSoundPlaying(m_bgmGame)) {
              m_sfxManager.PlaySound(m_bgmGame, -1);
              m_currentBGM = m_bgmGame;
            }
          }
        }

        if ((!m_skipPermissionCountOnMenu) || (index != 0)) {
          m_permissionController.AddChangeSceneCount();
        }

        //dont show ads if purchased alr
        if (ShouldShowAds()) {
          // if hide interstitial in menu        
          if ((index == 0) && (m_hideInterstitialOnMenu)) {
            return;
          }

          //add interval every time scene changes
          m_interstitialManager.AddInterval();
        }
      }

      void InitGamingServices() {
        try {
          var options = new InitializationOptions().SetEnvironmentName("production");
          UnityServices.InitializeAsync(options).ContinueWith(task => { m_initGameServicesDone = true; });
        } catch (Exception exception) {
          Debug.Log(exception.Message);
        }
      }

      void InitStore(List<StoreProductData> prods) {
        if (m_iapOn) {
          if (!m_store.HasInitialized) {
            Debug.Log("init store!");
            m_store.InitStore(prods, StoreType.Unity);
          }
        }
      }
    }
  }
}