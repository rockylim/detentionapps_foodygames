﻿using Caviezel.Core;
using Caviezel.Monetization;
using Caviezel.Pretend;
using Caviezel.Serialization;
using UnityEngine;


namespace Detentionapps {
  namespace FoodyGames {

    public class FoodyGameIAPTracker : PtdComponentActive, IUpdateableComponent {
      FoodyGameSystem m_system;
      PtdPersister m_persister;


      public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
        base.InitComponent(sceneStarter, gameController);

        // get main system
        m_system = sceneStarter.Engine.GetSystem<FoodyGameSystem>();
        Debug.Assert(null != m_system);

        //get persister
        m_persister = sceneStarter.Engine.GetSystem<GamePersisters>().GetPersister<PtdPersister>();
        Debug.Assert(null != m_persister);
      }

      public override void OnStartGame() {
        base.OnStartGame();
        if (m_system.m_iapOn) {
          for (int i = 0; i < m_system.m_IAPProduct.Count; i++) {
            if (m_persister.ProductExists(m_system.m_IAPProduct[i].m_id)) {
              BaseSenderEv ev_send = new BaseSenderEv(this.gameObject.name, m_system.m_IAPProduct[i].m_id);
              BroadcastEvent(ev_send);
            }
          }
        } else {
          BaseSenderEv ev_send = new BaseSenderEv(this.gameObject.name, "ev_noIAP");
          BroadcastEvent(ev_send);
        }
      }

      #region IUpdateable
      public void UpdateComponent(float dt) {
        if (IsActive) {
          if (m_system.GetPurchashedIAP() != "") {
            BaseSenderEv ev_send = new BaseSenderEv(this.gameObject.name, m_system.GetPurchashedIAP());
            BroadcastEvent(ev_send);
          }
        }
      }
      #endregion      
    }
  }
}