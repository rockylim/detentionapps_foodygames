﻿using Caviezel.Core;
using Caviezel.Pretend;
using UnityEngine;

namespace Detentionapps {
  namespace FoodyGames {
    public class FoodyGameAdCounter : PtdComponentActive {
      public BaseReceiverEv[] m_incrementEvs;

      FoodyGameSystem m_system;

      public override void InitComponent(ISceneStarter sceneStarter, IGameController gameController) {
        base.InitComponent(sceneStarter, gameController);
        //get system
        m_system = sceneStarter.Engine.GetSystem<FoodyGameSystem>();
        Debug.Assert(null != m_system);
      }

      public override bool OnReceiveEvent(BaseSenderEv ev, EventScope scope) {
        base.OnReceiveEvent(ev, scope);
        if (IsActive) {
          for (int i = 0; i < m_incrementEvs.Length; ++i) {
            if (m_incrementEvs[i].Matches(ev, scope)) {
              //do increment ad counter
              m_system.IncrementAdCounter();
              return true;
            }
          }
        }
        return false;
      }
    }
  }
}